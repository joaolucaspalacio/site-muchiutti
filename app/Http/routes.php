<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

Route::get('/', ['as'=>'home','uses'=>'HomeController@index']);
Route::get('/sobre-nos.html', ['as'=>'sobrenos','uses'=>'HomeController@sobrenos']);
Route::get('/pos-venda-ford.html', ['as'=>'posvenda','uses'=>'HomeController@posvenda']);
Route::get('/veiculos-novos.html', ['as'=>'novos','uses'=>'HomeController@novos']);
Route::get('/veiculos-semi-novos.html', ['as'=>'usados','uses'=>'HomeController@usados']);
Route::get('/{slug}/{codigo}.html', ['as'=>'interna','uses'=>'HomeController@veiculo']);
Route::get('/modelo/{slug}/{codigo}.html', ['as'=>'modelos','uses'=>'HomeController@novosmodelo']);
Route::get('/modelo/{slug}/{codigo}/{carro}/{id}.html', ['as'=>'carronovo','uses'=>'HomeController@carronovo']);
Route::get('/contato.html', ['as'=>'contato','uses'=>'HomeController@contato']);
Route::get('/proposta.html', ['as'=>'pdf','uses'=>'HomeController@pdf']);


Route::get('/noticias.html', ['as'=>'noticia','uses'=>'HomeController@noticias']);
Route::get('/noticias/{slug}/{codigo}.html', ['as'=>'noticiainterna','uses'=>'HomeController@noticiainterna']);


Route::get('/calcular', ['as'=>'calcular','uses'=>'HomeController@calculadora']);

Route::post('/entrecontato.html', ['as'=>'entrecontato','uses'=>'HomeController@contatoForm']);

/*
Route::get('/sobre-nos', '');
Route::get('/veiculos-novos', '');
Route::get('/veiculos-usados', '');
Route::get('/pos-venda', '');
Route::get('/contato', '');

*/