<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use DB;
use Cache;
use Carbon\Carbon;
use Mail;
use App;
class HomeController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {



        $banners = DB::table('tblbanner')->where('status','=','1')->get();
        //Carros Lista
        if (!Cache::has('carros') || Cache::get('carros')==null) {
            $carros = DB::table('tblveiculo')
                ->join('tblcambio', 'tblcambio.codigo', '=', 'tblveiculo.codigocambio')
                ->join('tblmarca', 'tblmarca.codigo', '=', 'tblveiculo.codigomarca')
                ->join('tblcombustivel', 'tblcombustivel.codigo', '=', 'tblveiculo.codigocombustivel')
                ->join('tblmodelo', 'tblmodelo.codigo', '=', 'tblveiculo.codigomodelo')
                ->join('tblmodeloversao', 'tblmodeloversao.codigo', '=', 'tblveiculo.codigomodeloversao')
                ->select('tblveiculo.*', 'tblcambio.descricao as cambio', 'tblmodeloversao.descricao as modeloversao', 'tblmodelo.descricao as modelo', 'tblmarca.descricao as marca', DB::Raw('(select foto from tblveiculofotos where codigoveiculo=tblveiculo.codigo limit 1) as fotosalbum'))
                ->orderBy('tblveiculo.codigo', 'Desc')
                ->limit(15)
                ->get();
            Cache::put('carros', $carros,Carbon::now()->addMinutes(60));
        }else{
            $carros = Cache::get('carros');
        }

        $noticias = DB::table('tblnoticia')
                    ->orderBy('codigo','DESC')
                    ->limit(3)
                    ->get();

        $novos = DB::table('novos_modelo')
            ->orderBy('descricao','ASC')
            ->where('imagem',"!=","NULL")
            ->take(6)
            ->get();
        $novos1 = DB::table('novos_modelo')
            ->orderBy('descricao','ASC')
            ->where('imagem',"!=","NULL")
            ->take(6)
            ->skip(6)
            ->get();

        $novos2 = DB::table('novos_modelo')
            ->orderBy('descricao','ASC')
            ->where('imagem',"!=","NULL")
            ->take(6)
            ->skip(12)
            ->get();


        $marcas = DB::table('tblmarca')
            ->whereExists(function ($query) {
                $query->select(DB::raw(1))
                    ->from('tblveiculo')
                    ->where('status','=',1)
                    ->whereRaw('tblveiculo.codigomarca= tblmarca.codigo');
            })
            ->orderBy('descricao','ASC')
            ->get();

        $combustivel = DB::table('tblcombustivel')
            ->orderBy('descricao','ASC')
            ->get();

        $cambios = DB::table('tblcambio')
            ->orderBy('descricao','ASC')
            ->get();

        return view('home')->with([
            'banners'=>$banners,
            'carros'=>$carros,
            'novos'=>$novos,
            'novos1'=>$novos1,
            'novos2'=>$novos2,
            'marcas'=>$marcas,
            'combustivel'=>$combustivel,
            'cambios'=>$cambios,
            'noticias'=>$noticias
        ]);
    }

    public function noticias(){
        $noticias = DB::table('tblnoticia')
                    ->orderBy('codigo','DESC')
                    ->get();

        return view('noticias');

    }
    public function noticiainterna($slug,$id){
        $noticias = DB::table('tblnoticia')
                    ->where("id","=",$id)
                    ->orderBy('codigo','DESC')
                    ->limit(3)
                    ->get();
        return view('noticiainterna');                    
    }

    public function carronovo($slug,$codigo,$carro,$id){

        $db = DB::table('novos_catalogo')
            ->join('tblcambio', 'tblcambio.codigo', '=', 'cambiocodigo')
            ->join('novos_modelo', 'novos_modelo.id', '=', 'modeloid')
            ->join('tblcombustivel', 'tblcombustivel.codigo', '=', 'combustivelcodigo')
            ->where('ativo','=','1')
            ->where('novos_catalogo.id','=',$id)
            ->select('novos_catalogo.*','tblcombustivel.descricao as combustivel', 'tblcambio.descricao as cambio','novos_modelo.descricao as modelo')
            ->first();

        $fotos = DB::table('novos_catalogo_foto')
            ->where('catalogoid','=',$id)
            ->get();

        $cores = DB::table('novos_cor_modelo')
                    ->join('novos_cor','novos_cor.id','=','corid')
                    ->where('modeloid','=',$codigo)
                    ->distinct()
                    ->get();

        $bancos = DB::table('tblbanco')
            ->where('bcostatus','=',1)
            ->whereExists(function ($query)  use ($db)  {
                $query->select(DB::raw(1))
                    ->from('tbltabela')
                    ->whereRaw('tblbanco.bcoid=tbltabela.codigobanco')
                    ->where('codigoveiculotipo', '=',2)
                    ->where('anoinicio', '<=',date('Y')-1)
                    ->where('anofim', '>=', date('Y')+1);
            })
            ->get();


        return view('carronovo')->with([
            'carro'=>$db,
            'fotos'=>$fotos,
            'cores'=>$cores,
            'bancos'=>$bancos
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function sobrenos()
    {
        $marcas = DB::table('tblmarca')
            ->whereExists(function ($query) {
                $query->select(DB::raw(1))
                    ->from('tblveiculo')
                    ->where('status','=',1)
                    ->whereRaw('tblveiculo.codigomarca= tblmarca.codigo');
            })
            ->orderBy('descricao','ASC')
            ->get();

        $combustivel = DB::table('tblcombustivel')
            ->orderBy('descricao','ASC')
            ->get();

        $cambios = DB::table('tblcambio')
            ->orderBy('descricao','ASC')
            ->get();

        return view('sobrenos')->with([
            'marcas'=>$marcas,
            'combustivel'=>$combustivel,
            'cambios'=>$cambios
        ]);
    }


    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function contato()
    {
        return view('contato');
    }


    public function calculadora(Request $request)
    {

        $codigobanco = $request->get('codigobanco');
        $carro = $request->get('carro');
        $entrada = $request->get('entrada');
        $valortotal= $request->get('valortotal');
        $codigoveiculotipo = $request->get('codigoveiculotipo');//novo;



        if ($codigoveiculotipo == 2){

            $veiculo = DB::table('tblveiculo')
                ->where('codigo', '=', $carro)
                ->first();
            if (!isset($veiculo)){
                return response()->json(['error'=>'Sem tabela']);
            }
            $datafabrica = $veiculo->anomodelo;

        }else{
            $datafabrica = date('Y');
        }

        $porcent = floor($entrada*100/$valortotal);

        $tabela = DB::table('tbltabela')
            ->where('codigoveiculotipo', '=', $codigoveiculotipo)
            ->where('anoinicio', '<=', $datafabrica)
            ->where('anofim', '>=', $datafabrica)
            ->where('codigobanco', '=', $codigobanco)
            ->where('entrada', '<=', $porcent)
            ->orderBy('entrada','DESC')
            ->orderBy('prazo','ASC')
            ->select('*',DB::Raw('CAST( coeficienteprotecao AS CHAR )'))
            ->get();


        $porcEntrada = 0;
        $tb = [];
        if (isset($tabela)) {
            foreach ($tabela as $obj) {
                if ($obj->entrada > $porcEntrada) {
                    $porcEntrada = $obj->entrada;
                }
                if ($obj->entrada == $porcEntrada) {
                    $tb[] = $obj;
                }

            }


            $valorveiculo = $valortotal;
            $final = [];
            if (count($tb) > 0) {
                foreach ($tb as $obj) {
                    $total = ($valorveiculo - $entrada) + $obj->valortac;

                    $sem = $total * $obj->coeficiente;
                    $com = $total * $obj->coeficienteprotecao;
                    
                    $json = [];
                    $json['com'] = number_format($com, 2, ',', '.');
                    $json['banco'] = $codigobanco;
                    $json['sem'] = number_format($sem, 2, ',', '.');
                    $json['prazo'] = $obj->prazo;
                    $json['entrada'] = number_format($entrada,2,',','.');
                    $json['totalfinanciar'] = number_format(($valortotal - $entrada),2,',','.');
                    $final[] = $json;
                }
            }else{
                return response()->json(['error'=>'Sem tabela']);
            }
            session(['session_calculadora'=>serialize($final)]);
            return response()->json($final);
        }else{
            return response()->json(['error'=>'Sem tabela']);
        }

    }


    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function posvenda()
    {
        return view('posvenda');
    }



    public function novos(Request $request)
    {
        $novos = DB::table('novos_modelo')
                    ->orderBy('descricao','ASC')
                    ->where('imagem',"!=","NULL")
                    ->take(6)
                    ->get();
        $novos1 = DB::table('novos_modelo')
            ->orderBy('descricao','ASC')
            ->where('imagem',"!=","NULL")
            ->take(6)
            ->skip(6)
            ->get();

        $novos2 = DB::table('novos_modelo')
            ->orderBy('descricao','ASC')
            ->where('imagem',"!=","NULL")
            ->take(6)
            ->skip(12)
            ->get();

        return view('novos')->with([
            'novos'=>$novos,
            'novos1'=>$novos1,
            'novos2'=>$novos2
        ]);
    }

    public function novosmodelo(Request $request,$slug,$id)
    {



        if (Cache::has('modeloscarros') || Cache::get('modeloscarros')==null) {
            $db = DB::table('novos_catalogo')
                ->join('tblcambio', 'tblcambio.codigo', '=', 'cambiocodigo')
                ->join('tblcombustivel', 'tblcombustivel.codigo', '=', 'combustivelcodigo')
                ->join('novos_modelo', 'novos_modelo.id', '=', 'modeloid')
                ->where('modeloid','=',$id)
                ->where('ativo','=','1')
                ->select('novos_catalogo.*','novos_modelo.codigo as model','tblcombustivel.descricao as combustivel', 'tblcambio.descricao as cambio',  DB::Raw('(select foto from novos_catalogo_foto where id=novos_catalogo.fotopadrao limit 1) as fotosalbum'))
                ->orderBy('valor','ASC');

            $carros = $db
                ->get();

            Cache::put('modeloscarros', $carros,Carbon::now()->addMinutes(60));
        }else{
            $carros = Cache::get('modeloscarros');
        }


        return view('listamodelos')->with([
            'carros'=>$carros,
            'slug'=>$slug,
            'codigo'=>$id
        ]);
    }


    public function usados(Request $request)
    {
        session(['session_calculadora'=>null]);

        $page = 0;
        $loja = '';
        $marca = '';
        $cambio = '';
        $minvalor = '';
        $maxvalor = '';
        $minkm = '';
        $maxkm = '';
        $anomin = '';
        $anomax = '';
        $combustivel="";

        if ($request->has('filtro')){
            $page = $request->get("page");
            $loja = $request->get("loja");
            $marca = $request->get("marca");
            $cambio = $request->get("cambio");
            $minvalor = $request->get("valor-min");
            $maxvalor = $request->get("valor-max");
            $anomin = $request->get("ano-min");
            $anomax = $request->get("ano-max");
            $minkm = $request->get("km-min");
            $maxkm = $request->get("km-max");
            $combustivel=  $request->get("combustivel");
        }

        if ($request->get("ordem")!='')
            $ordem = $request->get("ordem");
        else
            $ordem = 'ASC';

        $marcas = DB::table('tblmarca')
                        ->whereExists(function ($query) {
                            $query->select(DB::raw(1))
                                ->from('tblveiculo')
                                ->where('status','=',1)
                                ->whereRaw('tblveiculo.codigomarca= tblmarca.codigo');
                        })
                        ->orderBy('descricao','ASC')
                        ->get();

        $cambios = DB::table('tblcambio')
            ->orderBy('descricao','ASC')
            ->get();

        $combustiveis = DB::table('tblcombustivel')
            ->orderBy('descricao','ASC')
            ->get();

        $modelo = DB::table('tblmodelo')
            ->orderBy('descricao','ASC')
            ->get();

        if (Cache::has('carros') || Cache::get('carros')==null) {
            $db = DB::table('tblveiculo')
                ->join('tblcambio', 'tblcambio.codigo', '=', 'tblveiculo.codigocambio')
                ->join('tblmarca', 'tblmarca.codigo', '=', 'tblveiculo.codigomarca')
                ->join('tblcombustivel', 'tblcombustivel.codigo', '=', 'tblveiculo.codigocombustivel')
                ->join('tblmodelo', 'tblmodelo.codigo', '=', 'tblveiculo.codigomodelo')
                ->join('tblmodeloversao', 'tblmodeloversao.codigo', '=', 'tblveiculo.codigomodeloversao')
                ->join('tblcor', 'tblcor.codigo', '=', 'tblveiculo.codigocor')
                ->where('codigoveiculotipo','=','2')
                ->where('status','=','1')
                ->select('tblveiculo.*','tblcombustivel.descricao as combustivel', 'tblcambio.descricao as cambio', 'tblmodeloversao.descricao as modeloversao', 'tblmodelo.descricao as modelo','tblcor.descricao as cor', 'tblmarca.descricao as marca', DB::Raw('(select foto from tblveiculofotos where codigoveiculo=tblveiculo.codigo limit 1) as fotosalbum'))
                ->orderBy('valorvenda', $ordem);

            if($loja!=''){
                $db = $db->where('codigoloja','=',$loja);
            }

            if($marca!=''){
                $db = $db->where('tblveiculo.codigomarca','=',$marca);
            }

            if($cambio!=''){
                $db = $db->where('tblveiculo.codigocambio','=',$cambio);
            }

            if($minvalor!=''){
                $db = $db->where('valorvenda','>=',$minvalor);
            }
            if($maxvalor!=''){
                $db = $db->where('valorvenda','<=',$maxvalor);
            }
            if($minkm!=''){
                $db = $db->where('km','>=',$minkm);
            }
            if($maxkm!=''){
                $db = $db->where('km','<=',$maxkm);
            }
            if($anomin!=''){
                $db = $db->where('anomodelo','>=',$anomin);
            }
            if($anomax!=''){
                $db = $db->where('anomodelo','<=',$anomax);
            }

            if($combustivel!=''){
                $db = $db->where('codigocombustivel','<=',$combustivel);
            }


            $count = $db->count();
            $carros = $db
                ->paginate(10);

            Cache::put('carros', $carros,Carbon::now()->addMinutes(60));
            Cache::put('count', $count,Carbon::now()->addMinutes(60));
        }else{
            $carros = Cache::get('carros');
            $count = Cache::get('count');
        }



        return view('lista')->with([
            'carros'=>$carros,
            'marcas'=>$marcas,
            'count'=>$count,
            'page'=>$page+1,
            'loja'=>$loja,
            'marcasel'=>$marca,
            'modelo'=>$modelo,
            'cambio'=>$cambio,
            'minvalor'=>$minvalor,
            'maxvalor'=>$maxvalor,
            'minkm'=>$minkm,
            'maxkm'=>$maxkm,
            'minano'=>$anomin,
            'maxano'=>$anomax,
            'ordem'=>$ordem,
            'cambios'=>$cambios,
            'combustiveis'=>$combustiveis,
            'combustivel'=>$combustivel
        ]);
    }



    public function pdf(Request $request){
        $pdf = App::make('dompdf.wrapper');


        $codigo = $request->get('codigo');
        $entrada = ($request->get('entrada')=='')? "0.00": $request->get('entrada');
        $banco = $request->get('banco');
        if ($request->get('tipo')==2) {
            $db = DB::table('tblveiculo')
                ->join('tblcambio', 'tblcambio.codigo', '=', 'tblveiculo.codigocambio')
                ->join('tblmarca', 'tblmarca.codigo', '=', 'tblveiculo.codigomarca')
                ->join('tblcombustivel', 'tblcombustivel.codigo', '=', 'tblveiculo.codigocombustivel')
                ->join('tblmodelo', 'tblmodelo.codigo', '=', 'tblveiculo.codigomodelo')
                ->join('tblmodeloversao', 'tblmodeloversao.codigo', '=', 'tblveiculo.codigomodeloversao')
                ->join('tblcor', 'tblcor.codigo', '=', 'tblveiculo.codigocor')
                ->where('codigoveiculotipo', '=', '2')
                ->where('status', '=', '1')
                ->select('tblveiculo.*', 'tblcombustivel.descricao as combustivel', 'tblcambio.descricao as cambio', 'tblmodeloversao.descricao as modeloversao', 'tblmodelo.descricao as modelo', 'tblcor.descricao as cor', 'tblmarca.descricao as marca', DB::Raw('(select foto from tblveiculofotos where codigoveiculo=tblveiculo.codigo limit 1) as fotosalbum'))
                ->where('tblveiculo.codigo', '=', $codigo)
                ->first();
            $opcionais = DB::table('tblveiculoopcional')
                ->join('tblopcional','tblopcional.codigo','=','tblveiculoopcional.codigoopcional')
                ->where('codigoveiculo','=',$codigo)
                ->get();

            $fotos = DB::table('tblveiculofotos')
                ->where('codigoveiculo','=',$codigo)
                ->get();


            if(!isset($db)){
                return;
            }

            $marca = $db->marca;
            $cambio = $db->cambio;
            $carro = $db->modeloversao;
            $modelo = $db->modelo;
            $anofabricacao = $db->anofabricacao;
            $anomodelo = $db->anomodelo;
            $combustivel = $db->combustivel;
            $garantia = $db->garantia;
            $km = $db->km;
            $valor = $db->valorvenda;
            $cor = $db->cor;
            $opcoes="";
            $modeloversao = $db->modeloversao;

            $foto = $db->fotosalbum;
            if (isset($opcionais)) {
                foreach ($opcionais as $obj)
                    $opcoes .= $obj->opcional . ', ';
            }




        }else{
            $db = DB::table('novos_catalogo')
                ->join('tblcambio', 'tblcambio.codigo', '=', 'cambiocodigo')
                ->join('novos_modelo', 'novos_modelo.id', '=', 'modeloid')
                ->join('tblcombustivel', 'tblcombustivel.codigo', '=', 'combustivelcodigo')
                ->where('ativo','=','1')
                ->where('novos_catalogo.id','=',$codigo)
                ->select('novos_catalogo.*','novos_modelo.descricao as modelo','tblcombustivel.descricao as combustivel', 'tblcambio.descricao as cambio',  DB::Raw('(select foto from novos_catalogo_foto where id=novos_catalogo.fotopadrao limit 1) as fotosalbum'))
                ->first();

            if(!isset($db)){
                return;
            }

            $opcionais = DB::table('tblveiculoopcional')
                ->join('tblopcional','tblopcional.codigo','=','tblveiculoopcional.codigoopcional')
                ->where('codigoveiculo','=',$codigo)
                ->get();



            $fotos = DB::table('novos_catalogo_foto')
                ->where('catalogoid','=',$codigo)
                ->get();

            $cores = DB::table('novos_cor_modelo')
                ->join('novos_cor','novos_cor.id','=','corid')
                ->where('modeloid','=',$codigo)
                ->distinct()
                ->get();

            $marca = 'Ford';
            $cambio = $db->cambio;
            $carro = $db->versao;
            $modelo = $db->modelo;
            $anofabricacao = $db->ano;
            $anomodelo = $db->ano;
            $combustivel = $db->combustivel;
            $garantia = $db->garantia;
            $km = 0;
            $valor = $db->valor;
            $cor = '';
            $opcoes="";
            $modeloversao = $db->versao;
            $foto = 'imgveiculos/'.$db->fotosalbum;
            if (isset($opcionais))
                foreach($opcionais as $obj)
                    $opcoes.='<div>'.$obj->opcional.'</br></div>';

        }


        $sem = "";
        $com = "";
        $banconome ="";
        $financiar =0;
        $entrada = 0.00;
        $financia = unserialize(session('session_calculadora'));
        if($financia!=null){
            foreach($financia as $obj){
                $banco = DB::table('tblbanco')
                    ->where('bcoid','=',$obj['banco'])
                    ->first();
                $banconome = $banco->bcodesc;
                $financiar = $obj['totalfinanciar'];
                $entrada = $obj['entrada'];
                $sem .= $obj['prazo'] ." Parcelas de R$".$obj['sem']."<br/>";
                $com .= $obj['prazo'] ." Parcelas de R$".$obj['com']."<br/>";
            }
        }
        

        $pdf->loadHTML('

        <html>
            <head>
                <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
                <style>
                    body{
                        font-family: verdana, sans-serif;
                    }
                    .page-break {
                        page-break-after: always;
                    }
                    .row{ width: 100%}
                    p{line-height: 1}
                    .left{float: left}
                    .center{text-align: center}
                    .borded{border: 1px solid black}
                    p{margin: 0; padding:2px;font-size: 12px;min-height:20px; }
                    .img{width:243px;height:220px}
                    .infos{width: 443px}
                    .strong {font-weight: bold}
                </style>
            </head>
            <body>
                <div class="row">
                    <div class="left" style="width: 180px;">
                          <img src="'.url('/images/logomuchiutti.png').'" style="width:100%"/>
                    </div>
                    <div  class="left" style="width: 490px;">
                        <h2 class="center">V.Muchiutt</h2>
                        <p class="center">
                        Av.Joaquim Constantino, 1895 - Presidente Prudente/Sp. - Telefone: (18)3918-2000<br/>
                        Rua Francisco Troncon, 10 - Adamantina/Sp. - Telefone: (18)3522-8848
                        </p>
                    </div>
                </div>
                <div style="clear:both"></div>
                <div class="row">

                    <h3 class="center">Veiculo</h3>

                    <p class="borded">'.$anomodelo.'/'.$anofabricacao.' '.$marca.'</p>
                    <p class="borded">'.$modelo.' '.$modeloversao.'</p>
                    <br/>
                    <div class="img left">
                          <img src="'.url($foto).'" style="width:100%;height:182px"/>
                    </div>
                    <div class="infos left">
                        <p class="borded center strong">INFORMAÇÕES DO VEÍCULO:</p>
                        <p class="borded"><b>VALOR:</b> R$ '.number_format($valor,2,',','.').'</p>
                        <p class="borded"><b>COR:</b> '.$cor.'</p>
                        <p class="borded"><b>CAMBIO:</b> '.$cambio.'</p>
                        <p class="borded"><b>VERSÃO:</b> '.$modeloversao.'</p>
                        <p class="borded"><b>COMBUSTIVEL:</b> '.$combustivel.'</p>
                        <p class="borded"><b>KM:</b> '.$km.'</p>
                        <p class="borded"><b>ANO MODELO:</b> '.$anomodelo.'</p>
                        <p class="borded"><b>ANO FABRICAÇÃO:</b> '.$anofabricacao.'</p>
                        <p class="borded"><b>MARCA:</b> '.$marca.'/'.$modelo.'</p>
                    </div>

                    <div style="clear:both"></div>
                    <p class="borded center strong">OPCIONAIS</p>
                    <p class="borded center" style="line-height:20px">'.$opcoes.'<br/></p>
                    <br/>
                    <p class="borded center strong">GARANTIA</p>
                    <p class="borded center">'.$garantia.'<br/></p>
                    <br/>
                    <p class="borded center strong">FINANCIAMENTO</p>

                    <div >
                        <p class="borded center">Valor do Veiculo R$: 13.990,00 Valor de Entrada ou Troca R$: '.$entrada.' Banco: '.$banconome.'</p>
                        <div class="left borded center" style="width:50%;;font-size:12px">
                            Sem Proteção<br/>
                            Total a Financiar R$:'.$financiar.'<br/>
                            '.$sem.'
                        </div>
                        <div class="left borded center" style="width:50%;font-size:12px">
                            Com Proteção<br/>
                            Total a Financiar R$:'.$financiar.'<br/>
                            '.$com.'
                        </div>
                    </div>

                    <div style="clear:both"></div>
                    <br/>

                    <br/><br/><br/><br/>
                    <p style="margin-top:10px" class="borded center">Os valores das parcelas acima são apenas simulações, sendo que as mesmas poderão apresentar variações conforme a tabela FIPE, Ano de Modelo e Juros de cada Banco. Na formalização do contrato, procure sempre o contato com o vendedor para obter os valores exatos.</p>


                </div>
            </body>
        </html>

        ');
        return $pdf->stream();
    }



    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function veiculo($slug,$codigo)
    {
        $db = DB::table('tblveiculo')
            ->join('tblcambio', 'tblcambio.codigo', '=', 'tblveiculo.codigocambio')
            ->join('tblmarca', 'tblmarca.codigo', '=', 'tblveiculo.codigomarca')
            ->join('tblcombustivel', 'tblcombustivel.codigo', '=', 'tblveiculo.codigocombustivel')
            ->join('tblmodelo', 'tblmodelo.codigo', '=', 'tblveiculo.codigomodelo')
            ->join('tblmodeloversao', 'tblmodeloversao.codigo', '=', 'tblveiculo.codigomodeloversao')
            ->join('tblcor', 'tblcor.codigo', '=', 'tblveiculo.codigocor')
            ->where('codigoveiculotipo','=','2')
            ->where('status','=','1')
            ->select('tblveiculo.*','tblcombustivel.descricao as combustivel', 'tblcambio.descricao as cambio', 'tblmodeloversao.descricao as modeloversao', 'tblmodelo.descricao as modelo','tblcor.descricao as cor', 'tblmarca.descricao as marca', DB::Raw('(select foto from tblveiculofotos where codigoveiculo=tblveiculo.codigo limit 1) as fotosalbum'))
            ->where('tblveiculo.codigo','=',$codigo)
            ->first();

        $opcionais = DB::table('tblveiculoopcional')
                        ->join('tblopcional','tblopcional.codigo','=','tblveiculoopcional.codigoopcional')
                        ->where('codigoveiculo','=',$codigo)
                        ->get();

        $fotos = DB::table('tblveiculofotos')
            ->where('codigoveiculo','=',$codigo)
            ->get();

        $bancos = DB::table('tblbanco')
                    ->where('bcostatus','=',1)
                    ->whereExists(function ($query)  use ($db)  {
                        $query->select(DB::raw(1))
                            ->from('tbltabela')
                            ->whereRaw('tblbanco.bcoid=tbltabela.codigobanco')
                            ->where('codigoveiculotipo', '=',2)
                            ->where('anoinicio', '<=', $db->anofabricacao)
                            ->where('anofim', '>=', $db->anofabricacao);
                    })
                    ->get();

        return view('carro')->with([
            'carro'=>$db,
            'opcionais'=>$opcionais,
            'fotos'=>$fotos,
            'bancos'=>$bancos
        ]);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function contatoForm(Request $request)
    {
        $email = $request->get('email');
        Mail::send('emails.contato', ['nome' => $request->get('nome'),'email' => $request->get('email'),'msg' => $request->get('msg')], function ($m) use ($email) {
            $m->from($email, 'Ford Muchiutt');
            $m->to("contato@palaciosoares.com.br", "Contato do Site")->subject('Contato do site!');
        });
        echo 1;
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
