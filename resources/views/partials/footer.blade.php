<!--Footer Start-->
<footer class="design_2">
    <div class="container">
        <div class="row">
            <div class="col-lg-4 col-md-4 col-sm-6 col-xs-12 padding-left-none md-padding-left-none sm-padding-left-15 xs-padding-left-15">
                <h4>newsletter</h4>
                <p>Se inscreva em nossa newsletter e fique por dentro de todos lançamentos e promoções da Ford</p>
                <form method="post" action="#" class="form_contact">
                    <input type="text" value="" name="MERGE0" placeholder="Email ">
                    <input type="submit" value="Subscribe" class="md-button">
                    <input type="hidden" name="u" value="">
                    <input type="hidden" name="id" value="">
                </form>
            </div>
            <div class="col-lg-4 col-md-4 col-sm-6 col-xs-12">
                <div class="fb-page" data-href="https://www.facebook.com/FORDVMUCHIUTT" data-width="320" data-height="10<div class="fb-page" data-href="https://www.facebook.com/FORDVMUCHIUTT" data-width="320" data-height="100" data-small-header="false" data-adapt-container-width="true" data-hide-cover="false" data-show-facepile="false" data-show-posts="false"><div class="fb-xfbml-parse-ignore"><blockquote cite="https://www.facebook.com/FORDVMUCHIUTT"><a href="https://www.facebook.com/FORDVMUCHIUTT">V. Muchiutt Veículos</a></blockquote></div></div>
        </div>
        <div class="col-lg-4 col-md-4 col-sm-6 col-xs-12 padding-right-none md-padding-right-none sm-padding-right-15 xs-padding-right-15">
            <h4>Nosso contato</h4>
            <div class="footer-contact">
                <ul>
                    <li><i class="fa fa-map-marker"></i> <strong>Endereço:</strong> Av Joaquim Constantino, 1895, Presidente Prudente - SP</li>
                    <li><i class="fa fa-phone"></i> <strong>Telefone:</strong>(18) 3918-2000</li>
                    <li><i class="fa fa-envelope-o"></i> <strong>Email:</strong><a href="mailto:veiculos@muchiutt.com.br">veiculos@muchiutt.com.br</a></li>
                </ul>

                <i class="fa fa-location-arrow back_icon"></i>
            </div>
        </div>
    </div>
    </div>
</footer>

<div class="clearfix"></div>
<section class="copyright-wrap padding-bottom-10">
    <div class="container">
        <div class="row">
            <div class="col-lg-4 col-md-4 col-sm-6 col-xs-12">
                <div class="logo-footer margin-bottom-20 md-margin-bottom-20 sm-margin-bottom-10 xs-margin-bottom-20"><a href="#">
                        <img src="images/logomuchiutti.png" style="width:120px"/>
                </div>
                <p>Copyright &copy; 2015 Muchiutti. Todos direitos reservados.</p>
            </div>
            <div class="col-lg-8 col-md-8 col-sm-6 col-xs-12">
                <ul class="social margin-bottom-25 md-margin-bottom-25 sm-margin-bottom-20 xs-margin-bottom-20 xs-padding-top-10 clearfix">
                    <li><a class="sc-1" href="#"></a></li>
                </ul>
            </div>
        </div>
    </div>
</section>
<div id="fb-root"></div>
<script>(function(d, s, id) {
        var js, fjs = d.getElementsByTagName(s)[0];
        if (d.getElementById(id)) return;
        js = d.createElement(s); js.id = id;
        js.src = "//connect.facebook.net/pt_BR/sdk.js#xfbml=1&version=v2.4&appId=189448654433457";
        fjs.parentNode.insertBefore(js, fjs);
    }(document, 'script', 'facebook-jssdk'));</script>
<div class="back_to_top"> <img src="{{url('images/arrow-up.png')}}" alt="scroll up" /> </div>
<!-- Bootstrap core JavaScript -->
<script src="{{url('js/retina.js')}}"></script>
<script type="text/javascript" src="{{url('js/jquery.parallax.js')}}"></script>
<script type="text/javascript" src="{{url('js/jquery.inview.min.js')}}"></script>
<script src="{{url('js/main.js')}}"></script>
<script type="text/javascript" src="{{url('js/jquery.fancybox.js')}}"></script>
<script src="{{url('js/modernizr.custom.js')}}"></script>
<script defer src="{{url('js/jquery.flexslider.js')}}"></script>
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jquery-maskmoney/3.0.2/jquery.maskMoney.min.js"></script>
<script src="{{url('js/jquery.bxslider.js')}}" type="text/javascript"></script>
<script src="{{url('js/jquery.selectbox-0.2.js')}}" type="text/javascript"></script>
<script type="text/javascript" src="{{url('js/jquery.mousewheel.js')}}"></script>
<script type="text/javascript" src="{{url('js/jquery.easing.js')}}"></script>

@yield('script')

<script type="text/javascript">

    $(function(e){

        $('.money').maskMoney();
        $('.nextSeta').on('click',function(e){
            $("#carousel-home").carousel("next");
        });

        $('.prevSeta').on('click',function(e){
            $("#carousel-home").carousel("prev");
        });

    });

</script>