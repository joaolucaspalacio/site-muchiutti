<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="shortcut icon" href="images/favicon.ico">
    <title>@yield('title') Ford Muchiutti - Carros Novos e usados em Presidente Prudente e Adamantina </title>
    <!-- Bootstrap core CSS -->
    <link href="{{url('css/bootstrap.min.css')}}" rel="stylesheet">

    <!-- HTML5 shiv and Respond.js IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
    <script src="{{url('js/html5shiv.js')}}"></script>
    <script src="{{url('js/respond.min.js')}}"></script>
    <![endif]-->

    <link rel="stylesheet" type="text/css" href="http://fonts.googleapis.com/css?family=Yellowtail%7COpen%20Sans%3A400%2C300%2C600%2C700%2C800" media="screen" />
    <!-- Custom styles for this template -->
    <link href="{{url('css/font-awesome.min.css')}}" rel="stylesheet" type="text/css" media="all">
    <link rel="stylesheet" href="{{url('css/flexslider.css')}}" type="text/css" media="screen" />
    <link rel="stylesheet" href="{{url('css/jquery.bxslider.css')}}" type="text/css" media="screen" />
    <link href="{{url('css/jquery.fancybox.css')}}" rel="stylesheet">
    <link href="{{url('css/jquery.selectbox.css')}}" rel="stylesheet">
    <link href="{{url('css/style.css')}}" rel="stylesheet">
    <link href="{{url('css/mobile.css')}}" rel="stylesheet">
    <link rel="stylesheet" type="text/css" href="{{url('css/settings.css')}}" media="screen" />
    <link href="{{url('css/animate.min.css')}}" rel="stylesheet">
    <link href="{{url('css/ts.css')}}" type="text/css" rel="stylesheet">
    <link href="{{url('css/sweetalert2.css')}}" type="text/css" rel="stylesheet">
    <script type="text/javascript" src="{{url('js/jquery.min.js')}}"></script>
    <script src="{{url('js/bootstrap.min.js')}}"></script>
    <script type="text/javascript" src="{{url('js/jquery.themepunch.tools.min.js')}}"></script>
    <script type="text/javascript" src="{{url('js/jquery.themepunch.revolution.min.js')}}"></script>
    <script type="text/javascript" src="{{url('js/wow.min.js')}}"></script>
    <script type="text/javascript" src="{{url('js/sweetalert2.min.js')}}"></script>
    <script type="text/javascript" src="https://maps.googleapis.com/maps/api/js?key&amp;sensor=false"></script>

    <!-- bxSlider Javascript file -->
    <script src="{{url('js/jquery.bxslider.min.js')}}"></script>
    <!-- bxSlider CSS file -->
    <link href="{{url('css/jquery.bxslider.css')}}" rel="stylesheet" />

    <meta name="csrf-token" content="{{ csrf_token() }}">
    <!-- Twitter Feed Scripts
         Uncomment to activate

    <script type="text/javascript" src="{{url('js/twitter/jquery.tweet.js')}}"></script>
    <script type="text/javascript" src="{{url('js/twitter/twitter_feed.js')}}"></script> -->

</head>
