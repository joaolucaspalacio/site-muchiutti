@extends('app')

@section('content')
    @include('common/menu',['active'=>'sobrenos'])
    <div class="clearfix"></div>

    <!--secondary-banner ends-->
    <div class="message-shadow"></div>
    <div class="clearfix"></div>
    <section class="content">
        <div class="container">
            <div class="inner-page">
                <div class="row">
                    <div class="col-lg-12 padding-left-none padding-right-none">
                        <p>
                            A V.Muchiutt, empresa prudentina , conhecida tradicionalmente por atuar no ramo de hotelaria e distribuição de auto-peças, tornou-se em 1999 distribuidora exclusiva na região de Presidente Prudente, de uma das maiores montadoras do mundo: a Ford Motors.
                        </p>
                        <p>
                            Além de gerar novos empregos e contribuir para o desenvolvimento econômico da região, a Ford V. Muchiutt trouxe a confiança e a tecnologia que a marca Ford representa em todo o mundo: Caminhões, utilitários, automóveis e as peças originais Ford.
                        </p>
                        <p>
                            Vários anos de sucesso e um longo caminho pela frente, resultado do trabalho de equipe e da parceria que a V. Muchiutt desenvolve com seus clientes, funcionários e fornecedores.
                        </p>
                    </div>
                </div>

                <div class="col-lg-12 col-md-12 margin-top-30 col-sm-12 col-xs-12 padding-right-none sm-padding-left-none md-padding-left-15 xs-padding-left-none padding-bottom-40 scroll_effect fadeInUp" data-wow-delay='.2s' style="z-index:100">
                    <h4 class="margin-bottom-25 margin-top-none"><strong>BUSCAR</strong> NOSSOS CARROS</h4>
                    <div class="search-form margin-top-20 padding-vertical-20">
                        <form method="get" action="{{route('usados')}}">
                            {{ csrf_field() }}
                            <div class="select-wrapper clearfix">
                                <div class="col-md-6">
                                    <div class="min-price select-dropdown">
                                        <div class="my-dropdown min-price-dropdown min-dropdown">
                                            <select name="ano-min" class="css-dropdowns" tabindex="1" >
                                                <option value="">Ano Min</option>
                                                <option value="2015">2015</option>
                                                <option value="2014">2014</option>
                                                <option value="2013">2013</option>
                                                <option value="2012">2012</option>
                                                <option value="2011">2011</option>
                                                <option value="2010">2010</option>
                                                <option value="2009">2009</option>
                                                <option value="2008">2008</option>
                                                <option value="2007">2007</option>
                                                <option value="2006">2006</option>
                                                <option value="2005">2005</option>
                                                <option value="2004">2004</option>
                                                <option value="2003">2003</option>
                                                <option value="2001">2002</option>
                                                <option value="2001">2001</option>
                                                <option value="2000">2000</option>
                                            </select>
                                        </div>
                                        <span class="my-dropdown">to</span>
                                        <div class="my-dropdown max-price-dropdown min-dropdown">
                                            <select name="ano-max" class="css-dropdowns" tabindex="1" >
                                                <option value="">Ano Max</option>
                                                <option value="2015">2015</option>
                                                <option value="2014">2014</option>
                                                <option value="2013">2013</option>
                                                <option value="2012">2012</option>
                                                <option value="2011">2011</option>
                                                <option value="2010">2010</option>
                                                <option value="2009">2009</option>
                                                <option value="2008">2008</option>
                                                <option value="2007">2007</option>
                                                <option value="2006">2006</option>
                                                <option value="2005">2005</option>
                                                <option value="2004">2004</option>
                                                <option value="2003">2003</option>
                                                <option value="2001">2002</option>
                                                <option value="2001">2001</option>
                                                <option value="2000">2000</option>
                                            </select>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="min-price select-dropdown">
                                        <div class="my-dropdown min-price-dropdown min-dropdown">
                                            <select name="valor-min" class="css-dropdowns" tabindex="1" >
                                                <option value="">Preço Min</option>
                                                <option>0</option>
                                                <option value="10000">&lt; 10,000</option>
                                                <option value="20000">&lt; 20,000</option>
                                                <option value="30000">&lt; 30,000</option>
                                                <option value="40000">&lt; 40,000</option>
                                                <option value="50000">&lt; 50,000</option>
                                                <option value="60000">&lt; 60,000</option>
                                                <option value="70000">&lt; 70,000</option>
                                                <option value="80000">&lt; 80,000</option>
                                                <option value="90000">&lt; 90,000</option>
                                                <option value="100000">+ 100,000</option>
                                            </select>
                                        </div>
                                        <span class="my-dropdown">to</span>
                                        <div class="my-dropdown max-price-dropdown min-dropdown">
                                            <select name="valor-max" class="css-dropdowns" tabindex="1" >
                                                <option value="">Preço Max</option>
                                                <option>0</option>
                                                <option value="10000">&lt; 10,000</option>
                                                <option value="20000">&lt; 20,000</option>
                                                <option value="30000">&lt; 30,000</option>
                                                <option value="40000">&lt; 40,000</option>
                                                <option value="50000">&lt; 50,000</option>
                                                <option value="60000">&lt; 60,000</option>
                                                <option value="70000">&lt; 70,000</option>
                                                <option value="80000">&lt; 80,000</option>
                                                <option value="90000">&lt; 90,000</option>
                                                <option value="100000">+ 100,000</option>
                                            </select>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="select-wrapper clearfix">
                                <div class="col-md-6">
                                    <div class="my-dropdown make-dropdown">
                                        <select name="loja" class="css-dropdowns" tabindex="1" >
                                            <option value="">Loja</option>
                                            <option value="1">Presidente Prudente</option>
                                            <option value="2">Adamantina</option>
                                        </select>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="min-price select-dropdown">
                                        <div class="my-dropdown min-price-dropdown min-dropdown">
                                            <select name="km-min" class="css-dropdowns" tabindex="1" >
                                                <option value="">KM Min</option>
                                                <option>0</option>
                                                <option value="10000">&lt; 10,000</option>
                                                <option value="20000">&lt; 20,000</option>
                                                <option value="30000">&lt; 30,000</option>
                                                <option value="40000">&lt; 40,000</option>
                                                <option value="50000">&lt; 50,000</option>
                                                <option value="60000">&lt; 60,000</option>
                                                <option value="70000">&lt; 70,000</option>
                                                <option value="80000">&lt; 80,000</option>
                                                <option value="90000">&lt; 90,000</option>
                                                <option value="100000">&lt; 100,000</option>
                                            </select>
                                        </div>
                                        <span class="my-dropdown">to</span>
                                        <div class="my-dropdown max-price-dropdown min-dropdown">
                                            <select name="km-max" class="css-dropdowns" tabindex="1" >
                                                <option value="">KM Max</option>
                                                <option>0</option>
                                                <option value="10000">&lt; 10,000</option>
                                                <option value="20000">&lt; 20,000</option>
                                                <option value="30000">&lt; 30,000</option>
                                                <option value="40000">&lt; 40,000</option>
                                                <option value="50000">&lt; 50,000</option>
                                                <option value="60000">&lt; 60,000</option>
                                                <option value="70000">&lt; 70,000</option>
                                                <option value="80000">&lt; 80,000</option>
                                                <option value="90000">&lt; 90,000</option>
                                                <option value="100000">&lt; 100,000</option>
                                            </select>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="select-wrapper clearfix">
                                <div class="col-md-6">
                                    <div class="my-dropdown make-dropdown">
                                        <select name="marca" class="css-dropdowns" tabindex="1" >
                                            <option value="">Marca</option>
                                            @if (isset($marcas))
                                                @foreach($marcas as $obj)
                                                    <option value="{{$obj->codigo}}">{{$obj->descricao}}</option>
                                                @endforeach
                                            @endif
                                        </select>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="my-dropdown make-dropdown">
                                        <select name="cambio" class="css-dropdowns" tabindex="1" >
                                            <option value="">Transmisão</option>
                                            @if (isset($cambios))
                                                @foreach($cambios as $obj)
                                                    <option value="{{$obj->codigo}}">{{$obj->descricao}}</option>
                                                @endforeach
                                            @endif
                                        </select>
                                    </div>
                                </div>
                            </div>
                            <div class="select-wrapper clearfix">
                                <div class="col-md-6">
                                    <div class="my-dropdown make-dropdown">
                                        <select name="combustivel" class="css-dropdowns" tabindex="1" >
                                            <option value="">Combustivel</option>
                                            @if (isset($combustivel))
                                                @foreach($combustivel as $obj)
                                                    <option value="{{$obj->codigo}}">{{$obj->descricao}}</option>
                                                @endforeach
                                            @endif
                                        </select>
                                    </div>
                                </div>
                                <input type="hidden" name="filtro" value="on">
                                <div class="col-md-6">
                                    <div class="my-dropdown">
                                        <input class="full-width" name="searchtext" type="search" value="" placeholder="Melhorar busca...">
                                    </div>
                                </div>
                            </div>
                            <div class="clear"></div>
                            <div class="select-wrapper clearfix">
                                <div class="form-element">
                                    <input type="submit" value="Buscar meu veiculo" class="find_new_vehicle pull-right md-button">
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
        <!--container ends-->
    </section>
    <!--content ends-->
    <div class="clearfix"></div>



@endsection