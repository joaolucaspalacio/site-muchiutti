@extends('app')

@section('content')

    @include('common/menu',['active'=>'contato'])


    <!--secondary-banner ends-->
    <div class="message-shadow"></div>
    <div class="clearfix"></div>
    <section class="content">
        <div class="container">
            <div class="inner-page">
                <div class="col-md-12 padding-none">
                    <!--MAP-->
                    <div class="find_map row clearfix">
                        <h2 class="margin-bottom-25 margin-top-none">NOSSAS LOJAS</h2>
                        <div class="map margin-vertical-30">
                            <div id='google-map-listing' class="contact" data-longitude='-51.407389' data-latitude='-22.141954' data-zoom='12' style='height: 390px;'></div>
                        </div>
                    </div>
                    <!--MAP-->
                    <!--CONTACT INFORMATION-->
                    <div class="row contacts margin-top-25">
                        <!--LEFT INFORMATION-->
                        <div class="col-md-6 left-information">
                            <div class="contact_information information_head clearfix">
                                <h3 class="margin-bottom-25 margin-top-none">PRESIDENTE PRUDENTE</h3>
                                <div class="address clearfix margin-right-25 padding-bottom-40">
                                    <div class="icon_address">
                                        <p><i class="fa fa-map-marker"></i><strong>Endereço:</strong></p>
                                    </div>
                                    <div class="contact_address">
                                        <p class="margin-bottom-none">
                                            Av Joaquim Constantino,1895 <br>
                                            Presidente Prudente - SP<br>
                                            Brasil</p>
                                    </div>
                                </div>
                                <div class="address clearfix address_details margin-right-25 padding-bottom-40">
                                    <ul class="margin-bottom-none">
                                        <li><i class="fa fa-phone"></i><strong>Telefone:</strong> <span>(18) 3918-2000</span></li>
                                        <li><i class="fa fa-envelope-o"></i><strong>Email:</strong> <a href="mailto:veiculos@muchiutt.com.br">veiculos@muchiutt.com.br</a></li>
                                    </ul>
                                </div>
                            </div>
                            <div class="contact_information information_head clearfix">
                                <h3 class="margin-bottom-25 margin-top-none">ADAMANTINA</h3>
                                <div class="address clearfix margin-right-25 padding-bottom-40">
                                    <div class="icon_address">
                                        <p><i class="fa fa-map-marker"></i><strong>Endereço:</strong></p>
                                    </div>
                                    <div class="contact_address">
                                        <p class="margin-bottom-none">
                                            Rua Franscisco Troncon,10 <br>
                                            Adamantina - SP <br>
                                            Brasil</p>
                                    </div>
                                </div>
                                <div class="address clearfix address_details margin-right-25 padding-bottom-40">
                                    <ul class="margin-bottom-none">
                                        <li><i class="fa fa-phone"></i><strong>Telefone:</strong> <span>(18) 3522-8848</span></li>
                                        <li><i class="fa fa-envelope-o"></i><strong>Email:</strong> <a href="mailto:veiculos@muchiutt.com.br">veiculos@muchiutt.com.br</a></li>
                                    </ul>
                                </div>
                                <div class="address clearfix address_details margin-right-25 padding-bottom-40">
                                    <ul class="margin-bottom-none">
                                        <li><i class="fa fa-"></i></li>
                                    </ul>

                                </div>
                            </div>
                            <!--INFORMATION HOUR-->

                            <!--INFORMATION HOUR-->
                        </div>
                        <!--LEFT INFORMATION-->

                        <!--RIGHT INFORMATION-->
                        <div class="col-md-5 col-lg-offset-1 col-md-offset-1 padding-right-none xs-padding-left-none sm-padding-left-none xs-margin-top-30">
                            <div class="contact_wrapper information_head">
                                <h3 class="margin-bottom-25 margin-top-none">ENTRE EM CONTATO</h3>
                                <div class="form_contact margin-bottom-20">
                                    <div id="result"></div>
                                    <fieldset id="contact_form">
                                        <input type="text" name="name" class="form-control margin-bottom-25" placeholder="Nome  (Required)" />
                                        <input type="email" name="email" class="form-control margin-bottom-25" placeholder="Email  (Required)" />
                                        <textarea name="msg" class="form-control margin-bottom-25 contact_textarea" placeholder="Sua mensagem..." rows="7"></textarea>
                                        <input id="submit_btn" type="submit" value="Enviar mensagem">
                                    </fieldset>
                                </div>
                            </div>
                        </div>
                        <!--RIGHT INFORMATION-->

                    </div>
                    <!---CONTACT INFORMATION-->

                </div>
                <div class="clearfix"></div>
            </div>
        </div>
        <!--container ends-->
    </section>
    <!--content ends-->
    <div class="clearfix"></div>
    <div class="back_to_top"> <img src="images/arrow-up.png" alt="scroll up" /> </div>


@endsection

@section('script')

    <script type="text/javascript">

        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });

        $(function(){
            $('#submit_btn').on('click',function(e){
                e.preventDefault();
                var nome = $("input[name=name]").val();
                var email = $("input[name=email]").val();
                var msg = $("textarea[name=msg]").val();

                if (nome == "" || email == "" || msg == ""){
                    sweetAlert('Oops...', 'Preencha todo os dados!', 'error');
                    return;
                }

                $('#submit_btn').val('Enviando...');
                $.ajax({
                    url: '{{route('entrecontato')}}',
                    error: function() {
                        sweetAlert('Oops...', 'Ocorreu um erro ao enviar!', 'error');
                        $('#submit_btn').val('Enviar mensagem');
                    },
                    data: {
                        nome:nome,
                        email: email,
                        msg:msg
                    },
                    success: function(data) {
                        swal(   'Enviado!',   'Em breve entraremos em contato!',   'success' )
                        $('#submit_btn').val('Enviar mensagem');
                        $("input[name=name]").val('');
                        $("input[name=email]").val('');
                        $("textarea[name=msg]").val('');
                    },
                    type: 'POST'
                });

            });
        });

    </script>

@endsection