
@extends('app')

@section('content')
    @include('common/menu',['active'=>'home'])

    <div class="clearfix"></div>
    <section class="banner-wrap">
        <div class="banner">
            <div class="tp-banner-container">
		<ul class="bxslider">
                 @if (isset($banners))
                    @foreach($banners as $obj)
                        <li style="background-size:cover;background-image:url('{{url($obj->imagem)}}');height:300px"></li>
                    @endforeach
                 @endif       
                </ul>
            </div>
            <script type="text/javascript">

           $(document).ready(function(){
              $('.bxslider').bxSlider();
            });
            </script>

            <!-- Content End -->

        </div>
    </section>
    <section class="message-wrap">
        <div class="container">
            <div class="row">
                <h2 class="col-lg-9 col-md-8 col-sm-12 col-xs-12 xs-padding-left-15">Conheça os melhores lançamentos do ano como o novo focus <span class="alternate-font">fastback</span></h2>
                <div class="col-lg-3 col-md-4 col-sm-12 col-xs-12 xs-padding-right-15"> <a href="#" class="default-btn pull-right action_button lg-button">Agendar o Test Drive</a> </div>
            </div>
        </div>
        <div class="message-shadow"></div>
    </section>
    <!--message-wrap ends-->
    <section class="content">
        <div class="container">
            <div class="inner-page homepage margin-bottom-none">
                <section class="car-block-wrap padding-bottom-40">
                    <div class="container">
                        <div class="row">
                            <div class="col-md-1 prevSeta"> </div>
                            <div id="carousel-home" class="carousel slide col-md-10" data-ride="carousel">


                                <ol class="carousel-indicators">
                                    <li data-target="#carousel-home" data-slide-to="0" class="active"></li>
                                    <li data-target="#carousel-home" data-slide-to="1" class=""></li>
                                </ol>


                                <!-- Wrapper for slides -->
                                <div class="carousel-inner" role="listbox">

                                    <div class="item active">
                                        <ul style="list-style: none">
                                            @if(isset($novos))
                                                @foreach($novos as $obj)
                                                    <li class="col-md-5 text-center">
                                                        <a href="{{route('modelos',['slug'=>str_slug($obj->descricao),'codigo'=>$obj->id])}}">
                                                            <img src="{{url('imgveiculos/'.$obj->imagem)}}" width="70%">
                                                            <h3>{{ucwords($obj->descricao)}}</h3>
                                                        </a>
                                                    </li>

                                                @endforeach
                                            @endif

                                        </ul>
                                    </div>

                                    <div class="item">
                                        <ul style="list-style: none">
                                            @if(isset($novos1))
                                                @foreach($novos1 as $obj)
                                                    <li class="col-md-5 text-center">
                                                        <a href="{{route('modelos',['slug'=>str_slug($obj->descricao),'codigo'=>$obj->id])}}">
                                                            <img src="{{url('imgveiculos/'.$obj->imagem)}}" width="70%">
                                                            <h3>{{ucwords($obj->descricao)}}</h3>
                                                        </a>
                                                    </li>

                                                @endforeach
                                            @endif

                                        </ul>
                                    </div>

                                    <div class="item">
                                        <ul style="list-style: none">
                                            @if(isset($novos2))
                                                @foreach($novos2 as $obj)
                                                    <li class="col-md-5 text-center">
                                                        <a href="{{route('modelos',['slug'=>str_slug($obj->descricao),'codigo'=>$obj->id])}}">
                                                            <img src="{{url('imgveiculos/'.$obj->imagem)}}" width="70%">
                                                            <h3>{{ucwords($obj->descricao)}}</h3>
                                                        </a>
                                                    </li>

                                                @endforeach
                                            @endif

                                        </ul>
                                    </div>


                                </div>

                            </div>
                            <div class="col-md-1 nextSeta">  </div>
                        </div>
                        <div class="row">
                            <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12 welcome padding-left-none padding-bottom-40 scroll_effect fadeInUp">
                                <h4 class="margin-bottom-25 margin-top-none"><strong>BEM VINDO</strong> A NOSSO NOVO SITE</h4>
                                <p>Seja bem-vindo ao nosso novo site.
                                    Inteiramente reformulado, ele traz a você uma experiência de navegação muito melhor e agradável,
                                    com um visual mais moderno e dinâmico. <br/>Aqui você poderá encontrar informações sobre nossos produtos
                                    e serviços e as últimas novidades da  <span class="alternate-font">Ford</span>.
                                </p><p>
                                    Esperamos que gostem da novidade e, não esqueçam de deixar os comentários.
                                </p>
                            </div>
                            <!--welcome ends-->
                            <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12 padding-right-none sm-padding-left-none md-padding-left-15 xs-padding-left-none padding-bottom-40 scroll_effect fadeInUp" data-wow-delay='.2s' style="z-index:100">
                                <h4 class="margin-bottom-25 margin-top-none"><strong>BUSCAR</strong> NOSSOS CARROS</h4>
                                <div class="search-form margin-top-20 padding-vertical-20">
                                    <form method="get" action="{{route('usados')}}">
                                        {{ csrf_field() }}
                                        <div class="select-wrapper clearfix">
                                            <div class="col-md-6">
                                                <div class="min-price select-dropdown">
                                                    <div class="my-dropdown min-price-dropdown min-dropdown">
                                                        <select id="anomin" class="css-dropdowns" tabindex="1" >
                                                            <option value="">Ano Min</option>
                                                            <option value="2016">2016</option>
                                                            <option value="2015">2015</option>
                                                            <option value="2014">2014</option>
                                                            <option value="2013">2013</option>
                                                            <option value="2012">2012</option>
                                                            <option value="2011">2011</option>
                                                            <option value="2010">2010</option>
                                                            <option value="2009">2009</option>
                                                            <option value="2008">2008</option>
                                                            <option value="2007">2007</option>
                                                            <option value="2006">2006</option>
                                                            <option value="2005">2005</option>
                                                            <option value="2004">2004</option>
                                                            <option value="2003">2003</option>
                                                            <option value="2001">2002</option>
                                                            <option value="2001">2001</option>
                                                            <option value="2000">2000</option>
                                                        </select>
                                                    </div>
                                                    <span class="my-dropdown">to</span>
                                                    <div class="my-dropdown max-price-dropdown min-dropdown">
                                                        <select id="anomax" class="css-dropdowns" tabindex="1" >
                                                            <option value="">Ano Max</option>
                                                            <option value="2015">2015</option>
                                                            <option value="2015">2015</option>
                                                            <option value="2014">2014</option>
                                                            <option value="2013">2013</option>
                                                            <option value="2012">2012</option>
                                                            <option value="2011">2011</option>
                                                            <option value="2010">2010</option>
                                                            <option value="2009">2009</option>
                                                            <option value="2008">2008</option>
                                                            <option value="2007">2007</option>
                                                            <option value="2006">2006</option>
                                                            <option value="2005">2005</option>
                                                            <option value="2004">2004</option>
                                                            <option value="2003">2003</option>
                                                            <option value="2001">2002</option>
                                                            <option value="2001">2001</option>
                                                            <option value="2000">2000</option>
                                                        </select>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-md-6">
                                                <div class="min-price select-dropdown">
                                                    <div class="my-dropdown min-price-dropdown min-dropdown">
                                                        <select id="minvalor" class="css-dropdowns" tabindex="1" >
                                                            <option value="">Preço Min</option>
                                                            <option>0</option>
                                                            <option value="10000">&lt; 10,000</option>
                                                            <option value="20000">&lt; 20,000</option>
                                                            <option value="30000">&lt; 30,000</option>
                                                            <option value="40000">&lt; 40,000</option>
                                                            <option value="50000">&lt; 50,000</option>
                                                            <option value="60000">&lt; 60,000</option>
                                                            <option value="70000">&lt; 70,000</option>
                                                            <option value="80000">&lt; 80,000</option>
                                                            <option value="90000">&lt; 90,000</option>
                                                            <option value="100000">+ 100,000</option>
                                                        </select>
                                                    </div>
                                                    <span class="my-dropdown">to</span>
                                                    <div class="my-dropdown max-price-dropdown min-dropdown">
                                                        <select id="maxvalor" class="css-dropdowns" tabindex="1" >
                                                            <option value="">Preço Max</option>
                                                            <option>0</option>
                                                            <option value="10000">&lt; 10,000</option>
                                                            <option value="20000">&lt; 20,000</option>
                                                            <option value="30000">&lt; 30,000</option>
                                                            <option value="40000">&lt; 40,000</option>
                                                            <option value="50000">&lt; 50,000</option>
                                                            <option value="60000">&lt; 60,000</option>
                                                            <option value="70000">&lt; 70,000</option>
                                                            <option value="80000">&lt; 80,000</option>
                                                            <option value="90000">&lt; 90,000</option>
                                                            <option value="100000">+ 100,000</option>
                                                        </select>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="select-wrapper clearfix">
                                            <div class="col-md-6">
                                                <div class="my-dropdown make-dropdown">
                                                    <select id="loja" class="css-dropdowns" tabindex="1" >
                                                        <option value="">Loja</option>
                                                        <option value="1">Presidente Prudente</option>
                                                        <option value="2">Adamantina</option>
                                                    </select>
                                                </div>
                                            </div>
                                            <div class="col-md-6">
                                                <div class="min-price select-dropdown">
                                                    <div class="my-dropdown min-price-dropdown min-dropdown">
                                                        <select id="kmmin" class="css-dropdowns" tabindex="1" >
                                                            <option value="">KM Min</option>
                                                            <option>0</option>
                                                            <option value="10000">&lt; 10,000</option>
                                                            <option value="20000">&lt; 20,000</option>
                                                            <option value="30000">&lt; 30,000</option>
                                                            <option value="40000">&lt; 40,000</option>
                                                            <option value="50000">&lt; 50,000</option>
                                                            <option value="60000">&lt; 60,000</option>
                                                            <option value="70000">&lt; 70,000</option>
                                                            <option value="80000">&lt; 80,000</option>
                                                            <option value="90000">&lt; 90,000</option>
                                                            <option value="100000">&lt; 100,000</option>
                                                        </select>
                                                    </div>
                                                    <span class="my-dropdown">to</span>
                                                    <div class="my-dropdown max-price-dropdown min-dropdown">
                                                        <select id="kmmax" class="css-dropdowns" tabindex="1" >
                                                            <option value="">KM Max</option>
                                                            <option>0</option>
                                                            <option value="10000">&lt; 10,000</option>
                                                            <option value="20000">&lt; 20,000</option>
                                                            <option value="30000">&lt; 30,000</option>
                                                            <option value="40000">&lt; 40,000</option>
                                                            <option value="50000">&lt; 50,000</option>
                                                            <option value="60000">&lt; 60,000</option>
                                                            <option value="70000">&lt; 70,000</option>
                                                            <option value="80000">&lt; 80,000</option>
                                                            <option value="90000">&lt; 90,000</option>
                                                            <option value="100000">&lt; 100,000</option>
                                                        </select>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="select-wrapper clearfix">
                                            <div class="col-md-6">
                                                <div class="my-dropdown make-dropdown">
                                                    <select id="marca" class="css-dropdowns" tabindex="1" >
                                                        <option value="">Marca</option>
                                                        @if (isset($marcas))
                                                            @foreach($marcas as $obj)
                                                                <option value="{{$obj->codigo}}">{{$obj->descricao}}</option>
                                                            @endforeach
                                                        @endif
                                                    </select>
                                                </div>
                                            </div>
                                            <div class="col-md-6">
                                                <div class="my-dropdown make-dropdown">
                                                    <select id="cambio" class="css-dropdowns" tabindex="1" >
                                                        <option value="">Transmisão</option>
                                                        @if (isset($cambios))
                                                            @foreach($cambios as $obj)
                                                                <option value="{{$obj->codigo}}">{{$obj->descricao}}</option>
                                                            @endforeach
                                                        @endif
                                                    </select>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="select-wrapper clearfix">
                                            <div class="col-md-6">
                                                <div class="my-dropdown make-dropdown">
                                                    <select id="combustivel" class="css-dropdowns" tabindex="1" >
                                                        <option value="">Combustivel</option>
                                                        @if (isset($combustivel))
                                                            @foreach($combustivel as $obj)
                                                                <option value="{{$obj->codigo}}">{{$obj->descricao}}</option>
                                                            @endforeach
                                                        @endif
                                                    </select>
                                                </div>
                                            </div>
                                            <input type="hidden" name="filtro" value="on">
                                            <div class="col-md-6">
                                                <div class="my-dropdown">
                                                    <input class="full-width" name="searchtext" type="search" value="" placeholder="Melhorar busca...">
                                                </div>
                                            </div>
                                        </div>
                                        <div class="clear"></div>
                                        <div class="select-wrapper clearfix">
                                            <div class="form-element">
                                                <input type="button" value="Buscar meu veiculo" class="find_new_vehicle pull-right md-button">
                                            </div>
                                        </div>
                                    </form>
                                </div>
                            </div>
                            <!--invetory ends-->
                        </div>

                        <div class="recent-vehicles-wrap margin-top-30 sm-padding-left-none padding-bottom-40">
                            <div class="row">
                                <div class="col-lg-2 col-md-2 col-sm-4 col-xs-12 recent-vehicles padding-left-none">
                                    <h5 class="margin-top-none">Ultimos Veiculos</h5>
                                    <p>Veja os ultimos veículos que temos em nosso estoque.</p>
                                    <div class="arrow3 clearfix margin-top-15 xs-margin-bottom-25" id="slideControls3"><span class="prev-btn"></span><span class="next-btn"></span></div>
                                </div>
                                <div class="col-md-10 col-sm-8 padding-right-none xs-padding-left-none">
                                    <div class="carasouel-slider3">
                                        @if (isset($carros))
                                            @foreach($carros as $obj)
                                                <div class="slide">
                                                    <div class="car-block">
                                                        <div class="img-flex">
                                                            <a href="{{route('interna',['slug'=> str_slug($obj->marca."-".$obj->modelo."-".$obj->modeloversao),'codigo'=>$obj->codigo]) }}">
                                                                <span class="align-center">
                                                                    <i class="fa fa-3x fa-plus-square-o"></i>
                                                                </span>
                                                            </a>
                                                            <img src="{{url($obj->fotosalbum)}}" alt="" class="img-responsive">
                                                        </div>
                                                        <div class="car-block-bottom">
                                                            <h6><strong>{{$obj->modeloversao}}</strong></h6>
                                                            <h6>{{$obj->modelo}} / {{$obj->cambio}}</h6>
                                                            <h5>R${{$obj->valorvenda}} </h5>
                                                        </div>
                                                    </div>
                                                </div>
                                            @endforeach
                                        @endif


                                    </div>
                                </div>
                            </div>
                        </div>

                    </div>
                </section>
                <section class="welcome-wrap padding-top-30 sm-horizontal-15">
                    @if(isset($noticias))
                        <h4 class="margin-bottom-25 margin-top-none"><strong>NOTÍCIAS</strong> FORD</h4>
                        <div class="row">
                            @foreach($noticias as $obj)
                                <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12 margin-bottom-none">
                                    <div class="flip margin-bottom-30">
                                        <div class="card">
                                            <div class="face front"><img class="img-responsive" src="uploads/{{url($obj->imagem)}}" alt=""></div>
                                            <div class="face back">
                                                <div class='hover_title'></div>
                                                <a href="#"><i class="fa fa-link button_icon"></i></a> <a href="images/car1-lrg.jpg" class="fancybox"><i class="fa fa-arrows-alt button_icon"></i></a> </div>
                                        </div>
                                    </div>
                                    <h4><a href="#">{{$obj->titulo}}</a></h4>
                                    <p class="margin-bottom-none">{{substr($obj->texto,0,150)}}</p>
                                </div>

                            @endforeach
                        </div>
                        @endif
                                <!-- Footer Map -->
                        <div class='fullwidth_element_parent margin-top-30 padding-bottom-40'>
                            <div id='google-map-listing' class='fullwidth_element' data-longitude='-51.407389' data-latitude='-22.141954' data-zoom='16' style='height: 390px;' data-scroll='false' data-style='[{"featureType":"landscape","elementType":"labels","stylers":[{"visibility":"off"}]},{"featureType":"transit","elementType":"labels","stylers":[{"visibility":"off"}]},{"featureType":"poi","elementType":"labels","stylers":[{"visibility":"off"}]},{"featureType":"water","elementType":"labels","stylers":[{"visibility":"off"}]},{"featureType":"road","elementType":"labels.icon","stylers":[{"visibility":"off"}]},{"stylers":[{"hue":"#F0F0F0"},{"saturation":-100},{"gamma":2.15},{"lightness":12}]},{"featureType":"road","elementType":"labels.text.fill","stylers":[{"visibility":"on"},{"lightness":24}]},{"featureType":"road","elementType":"geometry","stylers":[{"lightness":57}]}]'></div>
                        </div>
                        <div class="car-rate-block clearfix margin-top-30 padding-bottom-40">
                            <div class="col-lg-2 col-md-2 col-sm-6 col-xs-12 xs-margin-bottom-40 sm-margin-bottom-none padding-left-none scroll_effect bounceInLeft">
                                <div class="small-block clearfix">
                                    <h4 class="margin-bottom-25 margin-top-none">Financiamento.</h4>
                                    <a href="#"><span class="align-center"><i class="fa fa-tag fa-7x"></i></span></a> </div>
                            </div>
                            <div class="col-lg-2 col-md-2 col-sm-6 col-xs-12 xs-margin-bottom-40 sm-margin-bottom-none scroll_effect bounceInLeft" data-wow-delay=".2s">
                                <div class="small-block clearfix">
                                    <h4 class="margin-bottom-25 margin-top-none">Serviços.</h4>
                                    <a href="#"><span class="align-center"><i class="fa fa-cogs fa-7x"></i></span></a> </div>
                            </div>
                            <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12 padding-left-none padding-right-none hours_operation">
                                <div class="small-block clearfix">
                                    <h4 class="margin-bottom-25 margin-top-none">Que hora estamos aberto?</h4>
                                    <div class="row">
                                        <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12 xs-margin-bottom-40 sm-margin-bottom-40 md-margin-bottom-none scroll_effect bounceInUp" data-wow-delay=".4s">
                                            <table class="table table-bordered no-border font-13px margin-bottom-none">
                                                <thead>
                                                <tr>
                                                    <td colspan="2"><strong>Departamento de vendas</strong></td>
                                                </tr>
                                                </thead>
                                                <tbody>
                                                <tr>
                                                    <td>Seg:</td>
                                                    <td>8:00am - 18:00</td>
                                                </tr>
                                                <tr>
                                                    <td>Ter:</td>
                                                    <td>8:00am - 18:00</td>
                                                </tr>
                                                <tr>
                                                    <td>Qua:</td>
                                                    <td>8:00am - 18:00</td>
                                                </tr>
                                                <tr>
                                                    <td>Qui:</td>
                                                    <td>8:00am - 18:00</td>
                                                </tr>
                                                <tr>
                                                    <td>Sex:</td>
                                                    <td>8:00am - 18:00</td>
                                                </tr>
                                                <tr>
                                                    <td>Sab:</td>
                                                    <td>9:00am - 18:00</td>
                                                </tr>
                                                <tr>
                                                    <td>Dom:</td>
                                                    <td>Fechado</td>
                                                </tr>
                                                </tbody>
                                            </table>
                                        </div>
                                        <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12 xs-margin-bottom-40 sm-margin-bottom-none scroll_effect bounceInUp" data-wow-delay=".4s">
                                            <table class="table table-bordered no-border font-13px margin-bottom-none">
                                                <thead>
                                                <tr>
                                                    <td colspan="2"><strong>Departamento Serviços</strong></td>
                                                </tr>
                                                </thead>
                                                <tbody>
                                                <tr>
                                                    <td>Seg:</td>
                                                    <td>8:00am - 18:00</td>
                                                </tr>
                                                <tr>
                                                    <td>Ter:</td>
                                                    <td>8:00am - 18:00</td>
                                                </tr>
                                                <tr>
                                                    <td>Qua:</td>
                                                    <td>8:00am - 18:00</td>
                                                </tr>
                                                <tr>
                                                    <td>Qui:</td>
                                                    <td>8:00am - 18:00</td>
                                                </tr>
                                                <tr>
                                                    <td>Sex:</td>
                                                    <td>8:00am - 18:00</td>
                                                </tr>
                                                <tr>
                                                    <td>Sab:</td>
                                                    <td>9:00am - 18:00</td>
                                                </tr>
                                                <tr>
                                                    <td>Dom:</td>
                                                    <td>Fechado</td>
                                                </tr>
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-lg-2 col-md-2 col-sm-6 col-xs-12 xs-margin-bottom-40 sm-margin-bottom-none scroll_effect bounceInRight" data-wow-delay=".2s">
                                <div class="small-block clearfix">
                                    <h4 class="margin-bottom-25 margin-top-none">Sobre nós.</h4>
                                    <a href="#"><span class="align-center"><i class="fa fa-users fa-7x"></i></span></a> </div>
                            </div>
                            <div class="col-lg-2 col-md-2 col-sm-6 col-xs-12 xs-margin-bottom-40 sm-margin-bottom-none padding-right-none scroll_effect bounceInRight">
                                <div class="small-block clearfix">
                                    <h4 class="margin-bottom-25 margin-top-none">Contato.</h4>
                                    <a href="#"><span class="align-center"><i class="fa fa-map-marker fa-7x"></i></span></a> </div>
                            </div>
                        </div>
                </section>
                <!--welcome-wrap ends

                <div class="row parallax_parent margin-top-30">
                    <div class="parallax_scroll clearfix" data-velocity="-.5" data-offset="-300" data-no-repeat="true" data-image="images/parallax2.jpg">
                        <div class="overlay">
                            <div class="container">

                                <div class="row">

                                    <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12 padding-left-none margin-vertical-60">
                                        <i class="fa fa-car"></i>

                                    <span class="animate_number margin-vertical-15">
                                        <span class="number">2,000</span>
                                    </span>

                                        Carros Vendidos
                                    </div>

                                    <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12 margin-vertical-60">
                                        <i class="fa fa-money"></i>

                                    <span class="animate_number margin-vertical-15">
                                        $<span class="number">750,000</span>
                                    </span>

                                        Total economizado
                                    </div>

                                    <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12 margin-vertical-60">
                                        <i class="fa fa-users"></i>

                                    <span class="animate_number margin-vertical-15">
                                        <span class="number">100</span>%
                                    </span>

                                        Satisfação dos clientes
                                    </div>

                                    <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12 padding-right-none margin-vertical-60">
                                        <i class="fa fa-tint"></i>

                                    <span class="animate_number margin-vertical-15">
                                        <span class="number">3,600</span>
                                    </span>

                                        Serviços Automotivos
                                    </div>

                                </div>
                            </div>
                        </div>
                    </div>
                </div>-->
            </div>
        </div>
    </section>


@endsection

@section('script')

    <script type="text/javascript">

        $(function(){

            $(".find_new_vehicle").on('click',function(e){
                var order= "";
                $("#order option").each(function(){
                    if ($(this).is(":selected")){
                        order = ($(this).val());
                    }
                });
                var valor = $("#valor option:selected").val();
                $("#valor option").each(function(){
                    if ($(this).is(":selected")){
                        valor = ($(this).val());
                    }
                });
                var cambio =$("#cambio option:selected").val();
                $("#cambio option").each(function(){
                    if ($(this).is(":selected")){
                        cambio = ($(this).val());
                    }
                });
                var marca = $("#marca").prop('selected');
                $("#marca option").each(function(){
                    if ($(this).is(":selected")){
                        marca = ($(this).val());
                    }
                });
                var loja = $("#loja option:selected").val();
                $("#loja option").each(function(){
                    if ($(this).is(":selected")){
                        loja = ($(this).val());
                    }
                });
                var maxvalor = $("#maxvalor option:selected").val();
                $("#maxvalor option").each(function(){
                    if ($(this).is(":selected")){
                        maxvalor = ($(this).val());
                    }
                });
                var minvalor = $("#minvalor option:selected").val();
                $("#minvalor option").each(function(){
                    if ($(this).is(":selected")){
                        minvalor = ($(this).val());
                    }
                });
                var anomin = $("#anomin option:selected").val();
                $("#anomin option").each(function(){
                    if ($(this).is(":selected")){
                        anomin = ($(this).val());
                    }
                });
                var anomax = $("#anomax option:selected").val();
                $("#anomax option").each(function(){
                    if ($(this).is(":selected")){
                        anomax = ($(this).val());
                    }
                });
                var kmmin = $("#kmmin option:selected").val();
                $("#kmmin option").each(function(){
                    if ($(this).is(":selected")){
                        kmmin = ($(this).val());
                    }
                });
                var kmmax = $("#kmmax option:selected").val();
                $("#kmmax option").each(function(){
                    if ($(this).is(":selected")){
                        kmmax = ($(this).val());
                    }
                });
                var combustivel = $("#combustivel option:selected").val();
                $("#combustivel option").each(function(){
                    if ($(this).is(":selected")){
                        combustivel = ($(this).val());
                    }
                });

                window.open("veiculos-semi-novos.html?filtro=on&ordem="+order+"&loja="+loja+"&cambio="+cambio+"&marca="+marca+"&valor-min="+minvalor+"&valor-max="+maxvalor+"&km-min="+kmmin+"&km-max="+kmmax+"&ano-min="+anomin+"&ano-max="+anomax+"&combustivel="+combustivel,'_self');
                return false;
            });


        });

    </script>

@endsection
