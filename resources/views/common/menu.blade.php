<!--Header Start-->
<header  data-spy="affix" data-offset-top="1" class="clearfix">
    <section class="toolbar">
        <div class="container">
            <div class="row">
                <div class="col-lg-6 left_bar">
                    <ul class="left-none">
                        <!--<li><i class="fa fa-search"></i>
                            <input type="search" placeholder="Buscar no site..." class="search_box">
                        </li>-->
                    </ul>
                </div>
                <div class="col-lg-6 ">
                    <ul class="right-none pull-right company_info">
                        <li><a href="tel:1893182000"><i class="fa fa-phone"></i> (18) 3918-2000 Presidente Prudente-SP / (18) 3522-8848 Adamantina-SP</a></li>

                    </ul>
                </div>
            </div>
        </div>
        <div class="toolbar_shadow"></div>
    </section>
    <div class="bottom-header" >
        <div class="container">
            <nav class="navbar navbar-default" role="navigation">
                <div class="container-fluid">
                    <!-- Brand and toggle get grouped for better mobile display -->
                    <div class="navbar-header">
                        <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1"> <span class="sr-only">Toggle navigation</span> <span class="icon-bar"></span> <span class="icon-bar"></span> <span class="icon-bar"></span> </button>
                        <a class="navbar-brand" href="#"><span class="logo">
                            <img src="{{url('images/logomuchiutti.png')}}" style=""/>
                        </a> </div>

                    <!-- Collect the nav links, forms, and other content for toggling -->
                    <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                        <ul class="nav navbar-nav pull-right">
                            <li @if($active=='home') class="active" @endif><a href="{{route('home')}}">Home</a></li>
                            <li @if($active=='sobrenos') class="active" @endif><a href="{{route('sobrenos')}}">Sobre nós</a></li>
                            <li @if($active=='novos') class="active" @endif><a href="{{route('novos')}}">Veículos Novos</a></li>
                            <li @if($active=='usados') class="active" @endif><a href="{{route('usados')}}">Semi novos</a></li>
                            <li @if($active=='posvenda') class="active" @endif><a href="{{route('posvenda')}}">Pós Venda</a></li>
                            <li @if($active=='contato') class="active" @endif><a href="{{route('contato')}}">Fale Conosco</a></li>
                        </ul>
                    </div>
                    <!-- /.navbar-collapse -->
                </div>
                <!-- /.container-fluid -->
            </nav>
        </div>
        <div class="header_shadow"></div>
    </div>
</header>
<!--Header End-->
