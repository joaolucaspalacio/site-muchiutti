@extends('app')

@section('title')

@endsection

@section('content')

@include('common/menu',['active'=>'usados'])


        <!--secondary-banner ends-->
<div class="message-shadow"></div>
<div class="clearfix"></div>
<section class="content">
    <div class="container">
        <div class="inner-page row">

            <div class="clearfix"></div>
            <form method="post" action="#" class="listing_sort">
                <div class="select-wrapper listing_select clearfix margin-top-none margin-bottom-15 col-sm-12">
                    <div class="my-dropdown models-dropdown">
                        <select id="loja" class="css-dropdowns" tabindex="1" >
                            <option value="">Todas Lojas</option>
                            <option @if($loja==1) selected @endif value="1">Presidente Prudente</option>
                            <option @if($loja==2) selected @endif value="2">Adamantina</option>
                        </select>
                    </div>
                    <div class="my-dropdown body-styles-dropdown">
                        <select  id="marca" class="css-dropdowns" tabindex="1" >
                            <option value="">Todas Marcas</option>
                            @if(isset($marcas))
                                @foreach($marcas as $obj)
                                    <option  @if($marcasel==$obj->codigo) selected @endif value="{{$obj->codigo}}">{{$obj->descricao}}</option>
                                @endforeach
                            @endif
                        </select>
                    </div>
                    <div class="my-dropdown transmissions-dropdown">
                        <select id="cambio" class="css-dropdowns" tabindex="1" >
                            <option value="">Transmissão</option>
                            @if(isset($cambios))
                                @foreach($cambios as $obj)
                                    <option  @if($cambio==$obj->codigo) selected @endif value="{{$obj->codigo}}">{{$obj->descricao}}</option>
                                @endforeach
                            @endif
                        </select>
                    </div>
                    <div class="my-dropdown transmissions-dropdown">
                        <select id="combustivel" class="css-dropdowns" tabindex="1" >
                            <option value="">Combustível</option>
                            @if(isset($combustivel))
                                @foreach($combustiveis as $obj)
                                    <option  @if($combustivel==$obj->codigo) selected @endif value="{{$obj->codigo}}">{{$obj->descricao}}</option>
                                @endforeach
                            @endif
                        </select>
                    </div>
                    <div class="my-dropdown prices-dropdown">
                        <select id="minvalor" class="css-dropdowns" tabindex="1" >
                            <option value="">Preço Min</option>
                            <option @if($minvalor==10000) selected @endif   value="10000"> R$10,000</option>
                            <option @if($minvalor==20000) selected @endif  value="20000"> R$20,000</option>
                            <option @if($minvalor==30000) selected @endif  value="30000"> R$30,000</option>
                            <option @if($minvalor==40000) selected @endif  value="40000"> R$40,000</option>
                            <option @if($minvalor==50000) selected @endif  value="50000"> R$50,000</option>
                            <option @if($minvalor==60000) selected @endif  value="60000"> R$60,000</option>
                            <option @if($minvalor==70000) selected @endif  value="70000"> R$70,000</option>
                            <option @if($minvalor==80000) selected @endif  value="80000"> R$80,000</option>
                            <option @if($minvalor==90000) selected @endif  value="90000"> R$90.000</option>
                            <option @if($minvalor==200000) selected @endif  value="200000">+R$100.000</option>
                        </select>
                    </div>
                    <div class="my-dropdown prices-dropdown">
                        <select id="maxvalor" class="css-dropdowns" tabindex="1" >
                            <option value="">Preço Max</option>
                            <option @if($maxvalor==10000) selected @endif   value="10000"> R$10,000</option>
                            <option @if($maxvalor==20000) selected @endif  value="20000"> R$20,000</option>
                            <option @if($maxvalor==30000) selected @endif  value="30000"> R$30,000</option>
                            <option @if($maxvalor==40000) selected @endif  value="40000"> R$40,000</option>
                            <option @if($maxvalor==50000) selected @endif  value="50000"> R$50,000</option>
                            <option @if($maxvalor==60000) selected @endif  value="60000"> R$60,000</option>
                            <option @if($maxvalor==70000) selected @endif  value="70000"> R$70,000</option>
                            <option @if($maxvalor==80000) selected @endif  value="80000"> R$80,000</option>
                            <option @if($maxvalor==90000) selected @endif  value="90000"> R$90.000</option>
                            <option @if($maxvalor==200000) selected @endif  value="200000">+R$100.000</option>
                        </select>
                    </div>
                    <div class="my-dropdown prices-dropdown">
                        <select id="anomin" class="css-dropdowns" tabindex="1" >
                            <option value="">Ano Min</option>
                            <option @if($minano==2014) selected @endif   value="2014">2014</option>
                            <option @if($minano==2013) selected @endif   value="2013">2013</option>
                            <option @if($minano==2012) selected @endif   value="2012">2012</option>
                            <option @if($minano==2011) selected @endif   value="2011">2011</option>
                            <option @if($minano==2010) selected @endif   value="2010">2010</option>
                            <option @if($minano==2009) selected @endif   value="2009">2009</option>
                            <option @if($minano==2008) selected @endif   value="2008">2008</option>
                            <option @if($minano==2007) selected @endif   value="2007">2007</option>
                            <option @if($minano==2006) selected @endif   value="2006">2006</option>
                            <option @if($minano==2005) selected @endif   value="2005">2005</option>
                            <option @if($minano==2004) selected @endif   value="2004">2004</option>
                            <option @if($minano==2003) selected @endif   value="2003">2003</option>
                            <option @if($minano==2002) selected @endif   value="2002">2002</option>

                        </select>
                    </div>
                    <div class="my-dropdown prices-dropdown">
                        <select id="anomax" class="css-dropdowns" tabindex="1" >
                            <option value="">Ano Max</option>
                            <option @if($maxano==2016) selected @endif   value="2016">2016</option>
                            <option @if($maxano==2015) selected @endif   value="2015">2015</option>
                            <option @if($maxano==2014) selected @endif   value="2014">2014</option>
                            <option @if($maxano==2013) selected @endif   value="2013">2013</option>
                            <option @if($maxano==2012) selected @endif   value="2012">2012</option>
                            <option @if($maxano==2011) selected @endif   value="2011">2011</option>
                            <option @if($maxano==2010) selected @endif   value="2010">2010</option>
                            <option @if($maxano==2009) selected @endif   value="2009">2009</option>
                            <option @if($maxano==2008) selected @endif   value="2008">2008</option>
                            <option @if($maxano==2007) selected @endif   value="2007">2007</option>
                            <option @if($maxano==2006) selected @endif   value="2006">2006</option>
                            <option @if($maxano==2005) selected @endif   value="2005">2005</option>
                        </select>
                    </div>
                    <div class="my-dropdown prices-dropdown">
                        <select id="kmmin" class="css-dropdowns" tabindex="1" >
                            <option value="">KM</option>
                            <option @if($minkm==10000) selected @endif   value="10000"> 10,000</option>
                            <option @if($minkm==20000) selected @endif  value="20000"> 20,000</option>
                            <option @if($minkm==30000) selected @endif  value="30000"> 30,000</option>
                            <option @if($minkm==40000) selected @endif  value="40000"> 40,000</option>
                            <option @if($minkm==50000) selected @endif  value="50000"> 50,000</option>
                            <option @if($minkm==60000) selected @endif  value="60000"> 60,000</option>
                            <option @if($minkm==70000) selected @endif  value="70000"> 70,000</option>
                            <option @if($minkm==80000) selected @endif  value="80000"> 80,000</option>
                            <option @if($minkm==90000) selected @endif  value="90000"> 90.000</option>
                            <option @if($minkm==100000) selected @endif  value="100000"> 100.000</option>
                        </select>
                    </div>
                    <div class="my-dropdown prices-dropdown">
                        <select id="kmmax" class="css-dropdowns" tabindex="1" >
                            <option value="">Max KM</option>
                            <option @if($maxkm==10000) selected @endif   value="10000"> 10,000</option>
                            <option @if($maxkm==20000) selected @endif  value="20000"> 20,000</option>
                            <option @if($maxkm==30000) selected @endif  value="30000"> 30,000</option>
                            <option @if($maxkm==40000) selected @endif  value="40000"> 40,000</option>
                            <option @if($maxkm==50000) selected @endif  value="50000"> 50,000</option>
                            <option @if($maxkm==60000) selected @endif  value="60000"> 60,000</option>
                            <option @if($maxkm==70000) selected @endif  value="70000"> 70,000</option>
                            <option @if($maxkm==80000) selected @endif  value="80000"> 80,000</option>
                            <option @if($maxkm==90000) selected @endif  value="90000"> 90.000</option>
                            <option @if($maxkm==100000) selected @endif  value="100000"> 100.000</option>
                        </select>
                    </div>
                </div>
                <div class="select-wrapper pagination clearfix">
                    <div class="row">
                        <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12 sort-by-menu"> <span class="sort-by">Ordenar por:</span>
                            <div class="my-dropdown price-ascending-dropdown">
                                <select id="order" class="css-dropdowns" tabindex="1" >
                                    <option value="">Por preço</option>
                                    <option  @if($ordem=="ASC") selected @endif  value="ASC">Menor</option>
                                    <option  @if($ordem=="DESC") selected @endif value="DESC">Maior</option>
                                </select>
                            </div>
                        </div>
                        <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12 pull-right">
                            <ul class="form-links top_buttons">
                                <li><a href="#" class="gradient_button">Limpar filtros</a></li>
                            </ul>
                        </div>
                    </div>
                    <div class="col-md-12 align-center">
                        {!! $carros->appends([
                                    'filtro' => 'on',
                                    'cambio' => $cambio,
                                    'marca' => $marcasel,
                                    'loja' => $loja,
                                    'minvalor' => $minvalor,
                                    'minvalor' => $minvalor,
                                    'maxvalor' => $minvalor,
                                    'minano' => $minano,
                                    'maxano' => $maxano,
                                    'minkm' => $minkm,
                                    'maxkm' => $maxkm,
                                    'combustivel'=>$combustivel,
                                    'ordem' => $ordem
                        ])->render() !!}
                    </div>
                </div>
            </form>
            <div class="clearfix"></div>
            <div id="myTabContent" class="tab-content">
                <div class="tab-pane fade active in" id="full-width">
                    <div class="content-wrap">
                        <div class="row car_listings">
                            @if (isset($carros))
                                @foreach($carros as $obj)
                                    <div class="inventory margin-bottom-20 clearfix scroll_effect fadeIn">

                                        <a class="inventory" href="{{route('interna',['slug'=> str_slug($obj->marca."-".$obj->modelo."-".$obj->modeloversao),'codigo'=>$obj->codigo]) }}">
                                            <div class="title">{{$obj->modeloversao}}</div>
                                            @if (trim($obj->fotosalbum) !="")
                                                <img src="{{url($obj->fotosalbum)}}" class="preview" alt="preview">
                                            @endif
                                            <table class="options-primary">
                                                <tr>
                                                    <td class="option primary">Marca:</td>
                                                    <td class="spec">{{$obj->marca}}</td>
                                                </tr>
                                                <tr>
                                                    <td class="option primary">Modelo:</td>
                                                    <td class="spec">{{$obj->modelo}}</td>
                                                </tr>
                                                <tr>
                                                    <td class="option primary">KM Rodado:</td>
                                                    <td class="spec">{{number_format($obj->km,0,',','.')}} KM</td>
                                                </tr>
                                                <tr>
                                                    <td class="option primary">Trasnimissão:</td>
                                                    <td class="spec">{{$obj->cambio}}</td>
                                                </tr>
                                            </table>
                                            <table class="options-secondary">
                                                <tr>
                                                    <td class="option secondary">Cor Extrena:</td>
                                                    <td class="spec">{{$obj->cor}}</td>
                                                </tr>
                                                <tr>
                                                    <td class="option secondary">Ano:</td>
                                                    <td class="spec">{{$obj->anomodelo}} / {{$obj->anofabricacao}}</td>
                                                </tr>
                                                <tr>
                                                    <td class="option primary">Combustivel:</td>
                                                    <td class="spec">{{$obj->combustivel}}</td>
                                                </tr>


                                            </table>
                                            <div class="price"><b>a partir de:</b><br>
                                                <div class="figure">R$ {{number_format($obj->valorvenda,2,',','.')}}<br>
                                                </div>
                                                <div class="tax">Mais taxas</div>
                                            </div>
                                            <div class="view-details gradient_button"><i class='fa fa-plus-circle'></i> Ver detalhes </div>
                                            <div class="clearfix"></div>
                                        </a>

                                    </div>
                                @endforeach
                            @endif

                        </div>
                    </div>
                    <div class="col-md-12 align-center">
                        {!! $carros->appends([
                                    'filtro' => 'on',
                                    'cambio' => $cambio,
                                    'marca' => $marcasel,
                                    'loja' => $loja,
                                    'minvalor' => $minvalor,
                                    'minvalor' => $minvalor,
                                    'maxvalor' => $minvalor,
                                    'minano' => $minano,
                                    'maxano' => $maxano,
                                    'minkm' => $minkm,
                                    'maxkm' => $maxkm,
                                    'combustivel'=>$combustivel,
                                    'ordem' => $ordem
                        ])->render() !!}
                    </div>
                    <div class="clearfix"></div>
                </div>

            </div>
        </div>
    </div>
    <!--container ends-->
</section>
<!--content ends-->
<div class="clearfix"></div>
@endsection
@section('script')
    <script type="text/javascript">
        $(function(){
            $("select").on('change',function(e){
                var url      = window.location.pathname;
                var order= "";
                $("#order option").each(function(){
                    if ($(this).is(":selected")){
                        order = ($(this).val());
                    }
                });
                var valor = $("#valor option:selected").val();
                $("#valor option").each(function(){
                    if ($(this).is(":selected")){
                        valor = ($(this).val());
                    }
                });
                var cambio =$("#cambio option:selected").val();
                $("#cambio option").each(function(){
                    if ($(this).is(":selected")){
                        cambio = ($(this).val());
                    }
                });
                var marca = $("#marca").prop('selected');
                $("#marca option").each(function(){
                    if ($(this).is(":selected")){
                        marca = ($(this).val());
                    }
                });
                var loja = $("#loja option:selected").val();
                $("#loja option").each(function(){
                    if ($(this).is(":selected")){
                        loja = ($(this).val());
                    }
                });
                var maxvalor = $("#maxvalor option:selected").val();
                $("#maxvalor option").each(function(){
                    if ($(this).is(":selected")){
                        maxvalor = ($(this).val());
                    }
                });
                var minvalor = $("#minvalor option:selected").val();
                $("#minvalor option").each(function(){
                    if ($(this).is(":selected")){
                        minvalor = ($(this).val());
                    }
                });
                var anomin = $("#anomin option:selected").val();
                $("#anomin option").each(function(){
                    if ($(this).is(":selected")){
                        anomin = ($(this).val());
                    }
                });
                var anomax = $("#anomax option:selected").val();
                $("#anomax option").each(function(){
                    if ($(this).is(":selected")){
                        anomax = ($(this).val());
                    }
                });
                var kmmin = $("#kmmin option:selected").val();
                $("#kmmin option").each(function(){
                    if ($(this).is(":selected")){
                        kmmin = ($(this).val());
                    }
                });
                var kmmax = $("#kmmax option:selected").val();
                $("#kmmax option").each(function(){
                    if ($(this).is(":selected")){
                        kmmax = ($(this).val());
                    }
                });
                var combustivel = $("#combustivel option:selected").val();
                $("#combustivel option").each(function(){
                    if ($(this).is(":selected")){
                        combustivel = ($(this).val());
                    }
                });
                window.open("veiculos-semi-novos.html?filtro=on&ordem="+order+"&loja="+loja+"&cambio="+cambio+"&marca="+marca+"&valor-min="+minvalor+"&valor-max="+maxvalor+"&km-min="+kmmin+"&km-max="+kmmax+"&ano-min="+anomin+"&ano-max="+anomax+"&combustivel="+combustivel,'_self');
                return false;
            });
        });
    </script>
@endsection




