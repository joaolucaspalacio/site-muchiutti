
@extends('app')

@section('content')
    @include('common/menu',['active'=>'home'])
    <section class="content">
        <div class="container">
            <div class="inner-page homepage margin-bottom-none">
                    <section class="welcome-wrap padding-top-30 sm-horizontal-15">
                    
                        <h4 class="margin-bottom-25 margin-top-none"><strong>NOTÍCIAS</strong> FORD</h4>
                        <div class="row">
                           
                                <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12 margin-bottom-none">
                                    <div class="flip margin-bottom-30">
                                        <div class="card">
                                            <div class="face front"><img class="img-responsive" src="uploads/" alt=""></div>
                                            <div class="face back">
                                                <div class='hover_title'></div>
                                                <a href="#"><i class="fa fa-link button_icon"></i></a> <a href="images/car1-lrg.jpg" class="fancybox"><i class="fa fa-arrows-alt button_icon"></i></a> </div>
                                        </div>
                                    </div>
                                    <h4><a href="#">aaa</a></h4>
                                    <p class="margin-bottom-none">a</p>
                                </div>

                                <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12 margin-bottom-none">
                                    <div class="flip margin-bottom-30">
                                        <div class="card">
                                            <div class="face front"><img class="img-responsive" src="uploads/" alt=""></div>
                                            <div class="face back">
                                                <div class='hover_title'></div>
                                                <a href="#"><i class="fa fa-link button_icon"></i></a> <a href="images/car1-lrg.jpg" class="fancybox"><i class="fa fa-arrows-alt button_icon"></i></a> </div>
                                        </div>
                                    </div>
                                    <h4><a href="#">aaa</a></h4>
                                    <p class="margin-bottom-none">a</p>
                                </div>
                                <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12 margin-bottom-none">
                                    <div class="flip margin-bottom-30">
                                        <div class="card">
                                            <div class="face front"><img class="img-responsive" src="uploads/" alt=""></div>
                                            <div class="face back">
                                                <div class='hover_title'></div>
                                                <a href="#"><i class="fa fa-link button_icon"></i></a> <a href="images/car1-lrg.jpg" class="fancybox"><i class="fa fa-arrows-alt button_icon"></i></a> </div>
                                        </div>
                                    </div>
                                    <h4><a href="#">aaa</a></h4>
                                    <p class="margin-bottom-none">a</p>
                                </div>
                                <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12 margin-bottom-none">
                                    <div class="flip margin-bottom-30">
                                        <div class="card">
                                            <div class="face front"><img class="img-responsive" src="uploads/" alt=""></div>
                                            <div class="face back">
                                                <div class='hover_title'></div>
                                                <a href="#"><i class="fa fa-link button_icon"></i></a> <a href="images/car1-lrg.jpg" class="fancybox"><i class="fa fa-arrows-alt button_icon"></i></a> </div>
                                        </div>
                                    </div>
                                    <h4><a href="#">aaa</a></h4>
                                    <p class="margin-bottom-none">a</p>
                                </div>
                                <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12 margin-bottom-none">
                                    <div class="flip margin-bottom-30">
                                        <div class="card">
                                            <div class="face front"><img class="img-responsive" src="uploads/" alt=""></div>
                                            <div class="face back">
                                                <div class='hover_title'></div>
                                                <a href="#"><i class="fa fa-link button_icon"></i></a> <a href="images/car1-lrg.jpg" class="fancybox"><i class="fa fa-arrows-alt button_icon"></i></a> </div>
                                        </div>
                                    </div>
                                    <h4><a href="#">aaa</a></h4>
                                    <p class="margin-bottom-none">a</p>
                                </div>                                                                                                                                

                        
                        </div>
                    
                     
                </section>
            </div>
        </div>
    </section>


@endsection
