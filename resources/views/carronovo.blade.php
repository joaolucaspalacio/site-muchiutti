@extends('app')

@section('title')

@endsection

@section('content')

@include('common/menu',['active'=>''])

        <!--secondary-banner ends-->
<div class="message-shadow"></div>
<div class="clearfix"></div>
<section class="content">
    <div class="container">
        <div class="inner-page inventory-listing">
            <div class="inventory-heading margin-bottom-10 clearfix">
                <div class="row">
                    <div class="col-lg-10 col-md-10 col-sm-10 col-xs-12">
                        <h2>{{$carro->modelo or ''}} - {{$carro->versao or ''}}</h2>
                        <span class="margin-top-10">{{$carro->modelo or ''}} - 0 KM - Faça um Test-Drive</span> </div>
                    <div class="col-lg-2 col-md-2 col-sm-2 text-right">
                        <?php
                        $balance =  number_format($carro->valor,2,",",".");
                        ?>
                        <h2 class="valorfinal">R$ {{$balance or ''}}</h2>
                        <em>Mais taxas e licenciamento</em> </div>
                </div>
            </div>
            <div class="content-nav margin-bottom-30">
                <div class="row">
                    <!--<ul>

                        <li class="request gradient_button"><a href="#" class="fancybox_div">Pedir mais Info</a></li>
                        <li class="schedule gradient_button"><a href="#" class="fancybox_div">Marcar Test Drive</a></li>
                        <li class="offer gradient_button"><a href="#" class="fancybox_div">Fazer oferta</a></li>

                    </ul>-->
                </div>
            </div>
            <div class="row">
                <div class="col-lg-8 col-md-8 col-sm-8 col-xs-12 left-content padding-left-none">
                    <!--OPEN OF SLIDER-->
                    <div class="listing-slider">
                        <section class="slider home-banner">
                            <div class="flexslider" id="home-slider-canvas">
                                <ul class="slides">
                                    @if(isset($fotos))
                                        @foreach($fotos as $obj)
                                            <li data-thumb="{{url('imgveiculos/'.$obj->foto)}}"> <img src="{{url('imgveiculos/'.$obj->foto)}}" alt="" data-full-image="{{url('imgveiculos/'.$obj->foto)}}" /> </li>
                                        @endforeach
                                    @endif
                                </ul>
                            </div>
                        </section>
                        <section class="home-slider-thumbs">
                            <div class="flexslider" id="home-slider-thumbs">
                                <ul class="slides">
                                    @if(isset($fotos))
                                        @foreach($fotos as $obj)
                                            <li data-thumb="{{url('imgveiculos/'.$obj->foto)}}"> <img src="{{url('imgveiculos/'.$obj->foto)}}" alt="" data-full-image="{{url('imgveiculos/'.$obj->foto)}}" /> </li>
                                        @endforeach
                                    @endif
                                </ul>
                            </div>
                        </section>
                    </div>
                    <!--CLOSE OF SLIDER-->
                    <!--Slider End-->
                    <div class="clearfix"></div>
                    <div class="bs-example bs-example-tabs example-tabs margin-top-50">
                        <ul id="myTab" class="nav nav-tabs">
                            <li class="active"><a href="#vehicle" data-toggle="tab">Informações</a></li>
                            <li><a href="#features" data-toggle="tab">Opcionais</a></li>
                            <li><a href="#contact" data-toggle="tab">Entre em contato</a></li>
                        </ul>
                        <div id="myTabContent" class="tab-content margin-top-15 margin-bottom-20">
                            <div class="tab-pane fade in active" id="vehicle">
                                <h4>Descrição:</h4>
                                <p>{{$carro->descricao}}</p>
                                <h4>Garantia:</h4>
                                <p>{{$carro->garantia}}</p>
                            </div>
                            <div class="tab-pane fade" id="features">
                                <ul class="fa-ul">
                                    @if (isset($opcionais))
                                        @foreach($opcionais as $obj)
                                            <li><i class="fa-li fa fa-check"></i>{{$obj->opcional}}</li>
                                        @endforeach
                                    @endif
                                </ul>
                            </div>
                            <div class="tab-pane fade" id="contact">
                                <div class="contact_wrapper information_head">
                                    <h3 class="margin-bottom-25 margin-top-none">ENTRE EM CONTATO</h3>
                                    <div class="form_contact margin-bottom-20">
                                        <div id="result"></div>
                                        <fieldset id="contact_form">
                                            <input type="text" name="name" class="form-control margin-bottom-25" placeholder="Nome  (Required)">
                                            <input type="email" name="email" class="form-control margin-bottom-25" placeholder="Email  (Required)">
                                            <input type="email" name="telefone" class="form-control margin-bottom-25" placeholder="Telefone:">
                                            <textarea name="msg" class="form-control margin-bottom-25 contact_textarea" placeholder="Sua mensagem..." rows="7"></textarea>
                                            <input id="submit_btn" type="submit" value="Enviar mensagem">
                                        </fieldset>
                                    </div>
                                </div>
                            </div>


                        </div>
                    </div>
                    <div class="clearfix"></div>
                </div>
                <div class="col-lg-4 col-md-4 col-sm-4 right-content padding-right-none">
                    <div class="side-content">
                        <div class="car-info margin-bottom-50">
                            <div class="table-responsive">
                                <table class="table">
                                    <tbody>
                                    <tr>
                                        <td>TIPO:</td>
                                        <td>{{$carro->cambio}}</td>
                                    </tr>
                                    <tr>
                                        <td>ANO:</td>
                                        <td>{{$carro->ano }}</td>
                                    </tr>
                                    <tr>
                                        <td>COMBUSTIVEL:</td>
                                        <td>{{$carro->combustivel}}</td>
                                    </tr>
                                    <tr>
                                        <td>TORQUE:</td>
                                        <td>{{$carro->torque}}</td>
                                    </tr>
                                    <tr>
                                        <td>POTÊNCIA:</td>
                                        <td>{{$carro->potencia}}</td>
                                    </tr>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                        <div class="efficiency-rating text-center padding-vertical-15 margin-bottom-40">
                            <h3>Cores Disponíveis</h3>
                            <ul class="col-md-12 cores">
                                @if(isset($cores))
                                    @foreach($cores as $obj)
                                        @if($obj->imagem!="")
                                            <li class="" data-valor="{{$obj->valor}}" style="padding:3px;margin:0;width:50px;height:30px;background: url('{{url('imgveiculos/'.$obj->imagem)}}')">
                                                <img src="{{url('images/checked32.png')}}" class="corchecked" style="display: none"/>
                                            </li>
                                        @endif
                                    @endforeach
                                @endif
                            </ul>
                            <!--<p>Alguma legenda que precise.</p>-->
                        </div>

                        <div class="clearfix"></div>
                        <a class="btn-inventory" href="{{route('pdf')}}?codigo={{$carro->id}}&tipo=1" >Imprimir Proposta</a>
                        <div class="clear"></div>
                        <div class="financing_calculator margin-top-40">
                            <h3>CALCULADORA FINANCIAMENTO</h3>
                            <div class="table-responsive">
                                <table class="table no-border no-margin">
                                    <tbody>
                                    <tr>
                                        <td>Custo do veiculo ($):</td>
                                        <td><input type="text"   data-thousands="." data-decimal="," class="number money cost valortotal" placeholder="R$ 10.000,00" value="{{number_format($carro->valor,2,',','.')}}" /></td>
                                    </tr>
                                    <tr>
                                        <td>Valor da Entrada ($):</td>
                                        <td><input type="text" data-thousands="." data-decimal=","  class="number money entrada down_payment" value="0" /></td>
                                    </tr>
                                    </tbody>
                                </table>
                            </div>
                            <div class="bi_weekly clearfix">
                                <div class="pull-left">Banco:</div>
                                <div class="dropdown_container ">
                                    <select name="banco" class="frequency banco css-dropdowns">
                                        <option value=''>Escolha o banco</option>
                                        @if(isset($bancos))
                                            @foreach($bancos as $obj)
                                                <option value='{{$obj->bcoid}}'>{{$obj->bcodesc}}</option>
                                            @endforeach
                                        @endif
                                    </select>
                                </div>
                            </div>
                            <a class="btn-inventory pull-right calculate" href="#" id="calcula">Calcular</a>
                            <div class="clear"></div>
                            <div class="tablevalores" style="display: none">
                                <div class="table-responsive resultado">
                                    <h4>Financiamento</h4>
                                    <h6>Total financiar: R$<strong id="vtotal"></strong></h6>
                                    <h6>Entrada: <strong id="ventrada"></strong></h6>
                                    <table>
                                        <thead>
                                        <tr>
                                            <td>Prazo</td>
                                            <td>Sem proteção</td>
                                            <td>Com proteção</td>
                                        </tr>
                                        </thead>
                                        <tbody id="valores">

                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="clearfix"></div>

            <div class="clearfix"></div>
        </div>
    </div>
    <!--container ends-->
</section>
<!--content ends-->
<div class="clearfix"></div>


@endsection

@section('script')
    <script type="text/javascript">
        $(function(){

            $(".cores li").on('click',function(e){
                $(".corchecked").each(function(){
                    $(this).hide();
                });
                $(this).find('img').show();
                var valorcor = parseInt($(this).attr('data-valor'));
                var carro = parseInt("{{$carro->valor}}");
                $(".valorfinal").text("R$ "+formatReal((carro+valorcor).toFixed(2).replace(".","")));
                $(".valortotal").val(formatReal((carro+valorcor).toFixed(2).replace(".","")));

            });
        });
        function formatReal( int )
        {
            var tmp = int+'';
            tmp = tmp.replace(/([0-9]{2})$/g, ",$1");
            if( tmp.length > 6 )
                tmp = tmp.replace(/([0-9]{3}),([0-9]{2}$)/g, ".$1,$2");

            return tmp;
        }


        $('#submit_btn').on('click',function(e){
            e.preventDefault();
            var nome = $("input[name=name]").val();
            var email = $("input[name=email]").val();
            var msg = $("textarea[name=msg]").val();
            var telefone = $("input[name=telefone]").val();

            if (nome == "" || email == "" || msg == ""){
                sweetAlert('Oops...', 'Preencha todo os dados!', 'error');
                return;
            }

            $('#submit_btn').val('Enviando...');
            $.ajax({
                url: '{{route('entrecontato')}}',
                error: function() {
                    sweetAlert('Oops...', 'Ocorreu um erro ao enviar!', 'error');
                    $('#submit_btn').val('Enviar mensagem');
                },
                data: {
                    nome:nome,
                    email: email,
                    telefone: telefone,
                    msg:"   {{$carro->modelo}} /n" +msg
                },
                success: function(data) {
                    swal(   'Enviado!',   'Em breve entraremos em contato!',   'success' )
                    $('#submit_btn').val('Enviar mensagem');
                    $("input[name=name]").val('');
                    $("input[name=email]").val('');
                    $("textarea[name=msg]").val('');
                },
                type: 'POST'
            });

        });


        $(function(){
            $("#calcula").on('click',function(e){
                e.preventDefault();
                var banco    = $("select[name=banco] option:selected").val();
                var entrada  = $(".entrada").val().replace('.','').replace(',','.');
                var total    = $(".valortotal").val().replace('R$','').replace('.','').replace(',','.');
                $(".tablevalores").hide();
                if (banco=='') {
                    sweetAlert('Oops...', 'Escolha o banco', 'error');
                    return;
                }

                $.ajax({
                    dataType: "json",
                    url: "{{route('calcular')}}",
                    data: {
                        codigobanco:banco,
                        codigoveiculotipo:1,
                        valortotal:total,
                        entrada:entrada,
                    },
                    success: function(data){
                        if (data.error){
                            sweetAlert('Oops...', 'Não temos tabela de calculo para esse financiamento', 'error');
                        }else{
                            $("#ventrada").text("R$ "+entrada);
                            $("#vtotal").text(total);
                            $(".tablevalores").show();
                            $('#valores').empty();
                            $.each(data,function(i,item){
                                $("#ventrada").text("R$ "+item.entrada);
                                $("#vtotal").text("R$ "+item.totalfinanciar);
                                $('#valores').append('<tr><td>'+item.prazo+'</td><td>R$ '+item.sem+'</td><td class="text-left">R$ '+item.com+'</td></tr>');
                            })
                        }
                    }
                });
            });
        });

    </script>


@endsection