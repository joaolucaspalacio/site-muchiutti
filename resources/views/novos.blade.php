@extends('app')

@section('content')
    @include('common/menu',['active'=>'novos'])
    <!--message-wrap ends-->
    <section class="content">
        <div class="container">
            <div class="inner-page homepage margin-bottom-none">
                <section class="car-block-wrap padding-top-40">
                    <div class="container">
                        <div class="row">
                            <div class="col-md-1 prevSeta hidden-sm"> </div>
                            <div id="carousel-home" class="carousel slide col-md-10" data-ride="carousel">



                                <!-- Wrapper for slides -->
                                <div class="carousel-inner" role="listbox">


                                    <div class="item active">
                                        <ul style="list-style: none">
                                            @if(isset($novos))
                                                @foreach($novos as $obj)
                                                    <li class="col-md-5 text-center">
                                                        <a href="{{route('modelos',['slug'=>str_slug($obj->descricao),'codigo'=>$obj->id])}}">
                                                            <img src="{{url('imgveiculos/'.$obj->imagem)}}" width="70%">
                                                            <h3>{{ucwords($obj->descricao)}}</h3>
                                                        </a>
                                                    </li>

                                                @endforeach
                                            @endif

                                        </ul>
                                    </div>

                                    <div class="item">
                                        <ul style="list-style: none">
                                            @if(isset($novos1))
                                                @foreach($novos1 as $obj)
                                                    <li class="col-md-5 text-center">
                                                        <a href="{{route('modelos',['slug'=>str_slug($obj->descricao),'codigo'=>$obj->id])}}">
                                                            <img src="{{url('imgveiculos/'.$obj->imagem)}}" width="70%">
                                                            <h3>{{ucwords($obj->descricao)}}</h3>
                                                        </a>
                                                    </li>

                                                @endforeach
                                            @endif

                                        </ul>
                                    </div>

                                    <div class="item">
                                        <ul style="list-style: none">
                                            @if(isset($novos2))
                                                @foreach($novos2 as $obj)
                                                    <li class="col-md-5 text-center">
                                                        <a href="{{route('modelos',['slug'=>str_slug($obj->descricao),'codigo'=>$obj->id])}}">
                                                            <img src="{{url('imgveiculos/'.$obj->imagem)}}" width="70%">
                                                            <h3>{{ucwords($obj->descricao)}}</h3>
                                                        </a>
                                                    </li>

                                                @endforeach
                                            @endif

                                        </ul>
                                    </div>


                                </div>

                            </div>
                            <div class="col-md-1 nextSeta hidden-sm">  </div>
                        </div>

                    </div>
                </section>

            </div>
        </div>
    </section>


@endsection