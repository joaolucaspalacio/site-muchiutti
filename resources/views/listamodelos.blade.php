@extends('app')

@section('title')

@endsection

@section('content')

    @include('common/menu',['active'=>'novos'])


    <!--secondary-banner ends-->
    <div class="message-shadow"></div>
    <div class="clearfix"></div>
    <section class="content">
        <div class="container">
            <div class="inner-page row">

                <div class="clearfix"></div>
                <div id="myTabContent" class="tab-content">
                    <div class="tab-pane fade active in" id="full-width">
                        <div class="content-wrap">
                            <div class="row car_listings">
                                @if (isset($carros))
                                    @foreach($carros as $obj)
                                        <div class="inventory margin-bottom-20 clearfix scroll_effect fadeIn">

                                            <a class="inventory" href="{{route('carronovo',[
                                                                                            'slug'=>$slug,
                                                                                            'codigo'=>$codigo,
                                                                                            'carro'=>str_slug($obj->versao),
                                                                                            'id'=>$obj->id,
                                                                                            ]) }}">
                                                <div class="title">{{$obj->model}} {{$obj->versao}} CAT: {{$obj->codigo}}</div>
                                                @if (trim($obj->fotosalbum) !="")
                                                    <img src="{{url('imgveiculos/'.$obj->fotosalbum)}}" class="preview" alt="preview">
                                                @endif
                                                <table class="options-primary">
                                                    <tr>
                                                        <td class="option primary">Potência:</td>
                                                        <td class="spec">{{$obj->potencia}}</td>
                                                    </tr>
                                                    <tr>
                                                        <td class="option primary">Torque:</td>
                                                        <td class="spec">{{$obj->torque}}</td>
                                                    </tr>
                                                    <tr>
                                                        <td class="option primary">Transmissão:</td>
                                                        <td class="spec">{{$obj->cambio}}</td>
                                                    </tr>
                                                </table>
                                                <table class="options-secondary">
                                                    <tr>
                                                        <td class="option secondary">Ano:</td>
                                                        <td class="spec">{{$obj->ano}}</td>
                                                    </tr>
                                                    <tr>
                                                        <td class="option primary">Combustivel:</td>
                                                        <td class="spec">{{$obj->combustivel}}</td>
                                                    </tr>


                                                </table>
                                                <div class="price"><b>a partir de:</b><br>
                                                    <div class="figure">R$ {{number_format($obj->valor,2,',','.')}}<br>
                                                    </div>
                                                    <div class="tax">Mais taxas</div>
                                                </div>
                                                <div class="view-details gradient_button"><i class='fa fa-plus-circle'></i> Ver detalhes </div>
                                                <div class="clearfix"></div>
                                            </a>

                                        </div>
                                    @endforeach
                                @endif

                            </div>
                        </div>

                        <div class="clearfix"></div>
                    </div>

                </div>
            </div>
        </div>
        <!--container ends-->
    </section>
    <!--content ends-->
    <div class="clearfix"></div>


@endsection