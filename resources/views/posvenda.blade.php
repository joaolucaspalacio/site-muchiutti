@extends('app')

@section('content')

    @include('common/menu',['active'=>'posvenda'])

    <!--secondary-banner ends-->
    <div class="message-shadow"></div>
    <div class="clearfix"></div>
    <section class="content">
        <div class="container">
            <div class="inner-page services">

                <div class="row">
                    <div class="featured-service clearfix padding-bottom-40 margin-top-30">
                        <h2 class="margin-top-none">Serviços <strong>Ford para você</strong></h2>
                        <div class="row">
                            <div class="col-lg-4 col-md-4 col-sm-6 col-xs-12 padding-bottom-none scroll_effect fadeInUp">
                                <div class="featured">
                                    <h5></h5>
                                    <img src="images/android-icon.png" alt="" data-hoverimg="images/android-icon-hover.png">
                                    <p></p>
                                </div>
                            </div>
                            <div class="col-lg-4 col-md-4 col-sm-6 col-xs-12 padding-bottom-none scroll_effect fadeInUp" data-wow-delay=".2s">
                                <div class="featured">
                                    <h5></h5>
                                    <a href="https://www.agenda.ford.com/singleUser.do?acao=login&country=BRA" target="_blank">
                                        <img src="images/icon-windows-8.png" alt="" data-hoverimg="images/icon-windows-8-hover.png">
                                    </a>
                                    <p></p>
                                </div>
                            </div>
                            <div class="col-lg-4 col-md-4 col-sm-6 col-xs-12 padding-bottom-none scroll_effect fadeInUp" data-wow-delay=".4s">
                                <div class="featured">
                                    <h5></h5>
                                    <img src="images/twitter-icon.png" alt="" data-hoverimg="images/twitter-icon-hover.png">
                                    <p></p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>




                <div class="row">
                    <div class="detail-service clearfix margin-top-30 padding-bottom-40">
                        <h2 class="margin-top-none">Detalhes dos nossos <strong>Serviços</strong></h2>
                        <div class="row">
                            <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12 scroll_effect fadeInLeft">
                                <div class="details margin-top-25">
                                    <h5 class="customize">Box Rápido Motorcraft</h5>
                                    <div class="padding-top-10">
                                        <p>
                                            O Box Rápido Motorcraft foi desenvolvido para pessoas como você, que não tem tempo a perder. Sempre que seu veículo precisar de serviços de manutenção básica e rotineira, o Box Rápido Motorcraft será oferecido, de forma que você tenha o seu veículo totalmente em ordem, num prazo de até 2 horas.
                                        </p>
                                        <p>
                                            Você poderá acompanhar o serviço junto ao mecânico ou aguardar em uma confortável sala de espera.
                                            Além da agilidade e transparência, você também poderá contar com preços justos e competitivos oferecidos através dos Kits Preço Fechado Ford.
                                        </p>
                                        <p>
                                            Veja abaixo os serviços disponíveis no Box Rápido Motorcraft:
                                        </p>
                                        <ul>

                                            <li>Alinhamento e balanceamento;</li>
                                            <li>Óleo Lubrificante;</li>
                                            <li>Pastilhas e Discos;</li>
                                            <li>Baterias;</li>
                                            <li>Fluídos;</li>
                                            <li>Suspensão (Molas e Amortecedores);</li>
                                            <li>Velas;</li>
                                            <li>Lâmpadas;</li>
                                            <li>Palhetas e Limpadores;</li>
                                            <li>Promoções Ford;</li>
                                            <li>Revisões;</li>
                                            <li>Filtros;</li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                            <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12 scroll_effect fadeInRight">
                                <div class="details margin-top-25">
                                    <h5 class="music">Serviço Total Ford</h5>
                                    <div class="padding-top-10">
                                        <p>O programa Serviço Total Ford foi criado pensando no seu bem-estar e tranquilidade. Ao visitar um de nossos Distribuidores Ford para serviços em garantia, revisões periódicas e manutenções, você encontra uma equipe especializada e muito bem treinada que vai atendê-lo com rapidez, cordialidade e transparência.
                                        </p><p>
                                            Além disso, um Consultor Técnico avaliará o seu carro em detalhes, na sua presença, onde esclarecerá o que precisa ser reparado, qual o valor destes serviços, além de estabelecer um prazo para entrega do veículo.
                                            Assim, você sai com a garantia de um serviço completo, atendendo os padrões de qualidade Ford que você e seu veículo merecem.
                                        </p>
                                        <p>
                                            Agende seu atendimento através da opção “Agenda Ford” (site da Ford) ou entre em contato direto com o Distribuidor Ford de sua preferência.
                                        </p>
                                        <p>
                                            Veja todas as vantagens e benefícios que o Programa Serviço Total Ford tem pra você:
                                        </p>
                                        <ul>
                                            <li>Conveniência de horário de atendimento com hora marcada.</li>
                                            <li>Recepção imediata e personalizada.</li>
                                            <li>Sabe com antecedência o quanto vai pagar.</li>
                                            <li>Informação sobre o andamento dos serviços.</li>
                                            <li>Veículo pronto e entregue no prazo combinado.</li>
                                            <li>Explicação sobre o serviço realizado e os custos envolvidos.</li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 scroll_effect fadeInUp">
                                <div class="details margin-top-25">
                                    <h5 class="awards">Serviço de Atendimento ao Cliente</h5>
                                    <div class="padding-top-10">
                                        <p>A V. Muchiutt oferece o Serviço de Atendimento ao Cliente, com veículo da empresa e motorista, pra levar até a residência, quando o veículo ficar na oficina para revisão e outros serviços.
                                        </p>
                                        <p>
                                            Telefones para agendamento:
                                        </p>
                                        <ul>
                                            <li>Presidente Prudente: (18) 3918-2040</li>
                                            <li>Adamantina: (18) 3522-8848 </li>
                                        </ul>

                                    </div>
                                </div>

                                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 scroll_effect fadeInLeft" data-wow-delay=".2s">
                                    <div class="details margin-top-25">
                                        <h5 class="work">Agend@Ford</h5>
                                        <p class="padding-top-10 margin-bottom-none">Aqui você acessa diretamente a agenda da Ford V.Muchiutt, escolhe dia, horário e até o consultor técnico que desejar. É rápido e prático.
                                            Clique Aqui</p>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 featured-brand-slider padding-left-none padding-right-none margin-top-30">
                            <div class="featured-brand">
                                <h3 class="margin-bottom-20">ALGUMAS MARCAS QUE TRABALHAMOS</h3>
                                <div class="arrow2" id="slideControls"><span class="prev-btn"></span><span class="next-btn"></span></div>
                                <div class="carasouel-slider featured_slider">
                                    <!--<div class="slide jquery scroll_effect fadeInUp"><a href="#">jquery</a></div>
                                    <div class="slide html5 scroll_effect fadeInUp" data-wow-delay=".1s"><a href="#">html5</a></div>
                                    <div class="slide my-sql scroll_effect fadeInUp" data-wow-delay=".2s"><a href="#">my-sql</a></div>
                                    <div class="slide javascript scroll_effect fadeInUp" data-wow-delay=".3s"><a href="#">javascript</a></div>
                                    <div class="slide wordpress scroll_effect fadeInUp" data-wow-delay=".4s"><a href="#">wordpress</a></div>
                                    <div class="slide php scroll_effect fadeInUp" data-wow-delay=".5s"><a href="#">php</a></div>
                                    <div class="slide jquery"><a href="#">jquery</a></div>
                                    <div class="slide html5"><a href="#">html5</a></div>
                                    <div class="slide my-sql"><a href="#">my-sql</a></div>
                                    <div class="slide javascript"><a href="#">javascript</a></div>
                                    <div class="slide wordpress"><a href="#">wordpress</a></div>
                                    <div class="slide php"><a href="#">php</a></div>-->
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!--container ends-->
    </section>
    <!--content ends-->




@endsection